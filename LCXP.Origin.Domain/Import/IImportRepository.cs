﻿using LCXP.Origin.Domain.Project.ActiveDirectory;
using System;
using System.Collections.Generic;

namespace LCXP.Origin.Domain.Import
{
    public interface IImportRepository
    {
        List<UserInventory> GetUserInventoryImportResult(Guid projectId);
        List<DeviceInventory> GetDeviceInventoryImportResult(Guid projectId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using LCXP.Origin.Domain.Project.ActiveDirectory;

namespace LCXP.Origin.Domain.Import
{
    public class ImportRepository : IImportRepository
    {
        private readonly OriginContext _originContext;

        public ImportRepository(OriginContext originContext)
        {
            _originContext = originContext;
        }

        public List<DeviceInventory> GetDeviceInventoryImportResult(Guid projectId)
        {
            var result = _originContext.DeviceInventory.ToList();
            return result;
        }

        public List<UserInventory> GetUserInventoryImportResult(Guid projectId)
        {
            var result = _originContext.UserInventory.ToList();
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace LCXP.Origin.Domain.Client
{
    public interface IClientRepository
    {
        List<Client> GetAllClients();
        Client GetClient(Guid id);
        Client CreateClient(Client client);
    }
}

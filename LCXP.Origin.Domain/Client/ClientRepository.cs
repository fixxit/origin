﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LCXP.Origin.Domain.Client
{
    public class ClientRepository : IClientRepository
    {
        private readonly OriginContext _originContext;

        public ClientRepository(OriginContext originContext)
        {
            _originContext = originContext;
        }

        public List<Client> GetAllClients()
        {
            var clients = _originContext.Clients.ToList();
            return clients;
        }

        public Client CreateClient(Client client)
        {
            var savedClient = _originContext.Clients.Add(client);
            _originContext.SaveChanges();
            return savedClient;
        }

        public Client GetClient(Guid id)
        {
            var client = _originContext.Clients.Find(id);
            return client;
        }
    }
}

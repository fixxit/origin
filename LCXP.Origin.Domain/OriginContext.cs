﻿using LCXP.Origin.Domain.Project;
using LCXP.Origin.Domain.Project.ActiveDirectory;
using System.Data.Entity;

namespace LCXP.Origin.Domain
{
    public class OriginContext : DbContext
    {
        public OriginContext(string connectionString) : base(connectionString) { }

        public DbSet<Client.Client> Clients { get; set; }
        public DbSet<Project.Project> Projects { get; set; }
        public DbSet<BusinessUnit> BusinessUnits { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        public DbSet<ClassificationRule> ClassificationRules { get; set; }
        public DbSet<QualificationRule> QualificationRules { get; set; }
        public DbSet<UserInventory> UserInventory { get; set; }
        public DbSet<OrganizationalUnit> OrganizationalUnit { get; set; }
        public DbSet<ActiveDirectoryGroup> ActiveDirectoryGroup { get; set; }
        public DbSet<OSConversions> OSConversions { get; set; }
        public DbSet<DeviceInventory> DeviceInventory { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client.Client>()
                .HasMany(c => c.Projects)
                .WithRequired(p => p.Client)
                .HasForeignKey(p => p.ClientId);

            modelBuilder.Entity<Project.Configuration>()
                .HasMany(p => p.BusinessUnits)
                .WithRequired(b => b.Configuration)
                .HasForeignKey(b => b.ConfigurationId);

            modelBuilder.Entity<Project.Project>()
                .HasMany(p => p.ClassificationRules)
                .WithRequired(c => c.Project)
                .HasForeignKey(c => c.ProjectId);

            modelBuilder.Entity<Project.Project>()
                .HasMany(p => p.QualificationRules)
                .WithRequired(q => q.Project)
                .HasForeignKey(q => q.ProjectId);

            modelBuilder.Entity<Project.Project>()
                .HasRequired(p => p.Configuration)
                .WithRequiredPrincipal(c => c.Project);

            modelBuilder.Entity<UserInventory>().Map(c => c.ToTable("dbo.UserInventory"));
            modelBuilder.Entity<QualificationRule>().Map(c => c.ToTable("project.QualificationRule"));
            modelBuilder.Entity<ClassificationRule>().Map(c => c.ToTable("project.ClassificationRule"));
            modelBuilder.Entity<OrganizationalUnit>().Map(c => c.ToTable("dbo.OrganizationalUnit"));
            modelBuilder.Entity<ActiveDirectoryGroup>().Map(c => c.ToTable("dbo.ActiveDirectoryGroup"));
            modelBuilder.Entity<OSConversions>().Map(c => c.ToTable("dbo.OSConversions"));
            modelBuilder.Entity<DeviceInventory>().Map(c => c.ToTable("dbo.DeviceInventory"));
        }
    }
}

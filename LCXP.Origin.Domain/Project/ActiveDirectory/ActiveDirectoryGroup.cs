﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LCXP.Origin.Domain.Project.ActiveDirectory
{
    public class ActiveDirectoryGroup
    {
        public long Id { get; set; }
        public string ContainerId { get; set; } // "CN" + Hex value (generated)
        public string AdGroupName { get; set; } // dependent on objectClass === "group"  AdGroup is equal to first CN value in distinguishedName (ask Rob about multiple CN values in distinguished name)
        public string ParentGroup { get; set; } // Is this actually used?

        public string DistinguishedName { get; set; } // distinguishedName
        public string OrganizationalUnit { get; set; } // dependent on objectClass === "organizationalUnit" built up using OU values separated by \\ from distinguishedName from right to left
        public string Domain { get; set; } // distinguishedNames DC values from left to right

        public bool MsEligibleGroup { get; set; } // yes if matches one of MS products (hardcoded in the VB scripts) no if not
public string Application { get; set; }
        public string Version { get; set; }

        // Ask about application and version blank columns

        public string Comments { get; set; } // description (ask about objectClass value)
        //public BusinessUnit BusinessUnit { get; set; }
        //public int AuditId { get; set; }
    }
}

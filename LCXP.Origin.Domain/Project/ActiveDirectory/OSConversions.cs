﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LCXP.Origin.Domain.Project.ActiveDirectory
{
    public class OSConversions
    {
        public long Id { get; set; }
        public string Source { get; set; }
        public string OSID { get; set; }
        public string Manufacturer { get; set; }
        public string ProductFamily { get; set; }
        public string Product { get; set; }
        public string ProductVersion { get; set; }
        public string IsLicensable { get; set; }
        public string Quantity { get; set; }
        public string Type { get; set; }
        public string LicenseMetric { get; set; }
    }
}

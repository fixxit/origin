﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LCXP.Origin.Domain.Project.ActiveDirectory
{
    [Table("project.ClassificationRule")]
    public class ClassificationRule
    {
        public long Id { get; set; }
        public string ClassificationId { get; set; }
        public string Type { get; set; }
        public string Operator { get; set; }
        public string Object { get; set; }
        public string Source { get; set; }
        public string SetRoleTo { get; set; }
        public string SetTypeTo { get; set; }
        public string SetClassTo { get; set; }
        public string SetStateTo { get; set; }

        //public BusinessUnit SetBusinessUnitTo { get; set; }
        
        //public string ClassificationTarget { get; set; }
        public RuleType RuleType { get; set; }
        public Guid? BusinessUnitId { get; set; }
        public Guid ProjectId { get; set; }
        public Project Project { get; set; }

        // Device specific
        public string SetBusinessUnitTo { get; set; }
        public string SetCityTo { get; set; }
        public string SetCountryTo { get; set; }
        public string SetContinentTo { get; set; }
        public string SetRegionTo { get; set; }
        public string ClassificationTarget { get; set; }
        public string DeviceType { get; set; }
        public string SetEnvironmentTo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace LCXP.Origin.Domain.Project.ActiveDirectory
{
    public class UserInventory
    {
        public long ID { get; set; }
        public string UserId { get; set; } // "U" + Hex value (generated)
        public string UserName { get; set; } // objectClass === "user" then Username = first part of principal name
        // if principal name is null then username is first CN of distinguishedName (lowercase)
        public string DisplayName { get; set; } // display name is first CN of distinguishedName
        public string PrincipalName { get; set; } // userPrincipalName
        public string Domain { get; set; } // first DN value of distinguishedName
        public string OrganizationalUnit { get; set; } // OU values of distinguishedName from right to left
        public string DistinguishedName { get; set; } // distinguishedName
        public string ADname { get; set; } // sn
        // AdGivenName -> givenName
        public string FirstName { get; set; } // givenName
        public string LastName { get; set; } // ADname formatted with each word starting with capital letter 
        public string MaidenName { get; set; } // ADName split if contains a dash, last name is taken
        public string ParentID { get; set; } // UserId
        public string NormalizedFullName { get; set; } // FirstName + LastName
        public string AnonymizedName { get; set; } // Normalizedfullname with first and last 5 characters visible
        public string MailboxAddress { get; set; } // mail
        public DateTime? LastActivityDate { get; set; } // lastLogonTimestamp
        public DateTime WhenCreatedDate { get; set; } // lastLogonTimestamp
        public int? DaysSinceLastActivity { get; set; } // date diff between last activity date and creation time
        public int? AgeOfRecord { get; set; } // date diff between collection date and creation date (collection date is in file name)
        public bool Disabled { get; set; } // ask when results in this field being true
        public DateTime CollectionDate { get; set; } // date in file name
        public string Metric { get; set; } // is this still used?
        public string UserRole { get; set; } // type (maybe msExchDeviceType) // classification rules
        public string UserType { get; set; } // classification rules
        public string UserClass { get; set; } // classification rules
        public string UserState { get; set; } // classification rules
        public string UserStatus { get; set; } // substate (user exceptions)
        public string Funnel { get; set; } // legacy?
        public string ClassificationRuleIds { get; set; } // configured rules
        public string QualificationRuleIds { get; set; } // configured exceptions
        public int? UserCount { get; set; } // configured exceptions
        public string Comments { get; set; } // description
        public string Observations { get; set; }
        //public BusinessUnit BusinessUnit { get; set; }
        //public int AuditId { get; set; }
        public string BusinessUnit { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Continent { get; set; }
        public string Region { get; set; }
    }
}

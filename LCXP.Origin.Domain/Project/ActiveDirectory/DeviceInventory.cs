﻿using System;
using System.Collections.Generic;

namespace LCXP.Origin.Domain.Project.ActiveDirectory
{
    public class DeviceInventory
    {
        public long Id { get; set; }
        public string DeviceId { get; set; } // "D" + Hex value of row
        public string DeviceName { get; set; } // name or cn
        public string FullyQualifiedDomainName { get; set; } // dNSHostName or part of DN
        public string Domain { get; set; } // DN
        public string Environment { get; set; } // classification rule
        public string MACAddress { get; set; } // legacy
        public string IPAddress { get; set; } // legacy
        public string OrganizationalUnit { get; set; } // objectClass === "computer" then same rule for other OU's apply
        public string UsersCurrentlyLoggedOn { get; set; } // legacy
        public string Agent { get; set; } // legacy
        public string Policy { get; set; } // legacy
        public string Manufacturer { get; set; } // read from Conversions.xlsx file OS conversions tab -> manufacturer
        public string OperatingSystem { get; set; } // read from Conversions.xlsx file VSA conversions tab -> product
        public string Version { get; set; } // read from Conversions.xlsx file VSA conversions tab -> version
        public string VirtualizationHost { get; set; } // legacy
        public DateTime? LastActivityDate { get; set; } // date diff between last activity date and creation time
        public DateTime WhenCreatedDate { get; set; }
        public long? DaysSinceLastActivity { get; set; } // date diff between collection en creation date
        public long AgeOfRecord { get; set; } // date diff between collection and creation date
        public bool? Disabled { get; set; } // ask when results in this field being true
        public DateTime CollectionDate { get; set; } // found in file
        public string DeviceRole { get; set; } // type in the lookup conversion table
        public string DeviceType { get; set; } // device classification rule
        public string DeviceClass { get; set; } // device classification rule
        public string DeviceState { get; set; } // device exception rule
        public string DeviceStatus { get; set; } // device exception rule
        public bool? Scanned { get; set; } // look at function in code
        public bool? OfficeEligibleDevice { get; set; } // device exception rule
        public bool? InstalledAfterEAExpiration { get; set; } // look at function in code
        public string ClassificationRuleIds { get; set; } // rules applied
        public string QualificationRuleIds { get; set; } // rules applied
        public string Comments { get; set; } // description
        public string Observations { get; set; } // device exception rule
        public string ObservationsType { get; set; }
        public string ExclusionReason { get; set; }
        public string Action { get; set; }
        //public BusinessUnit BusinessUnit { get; set; }
        //public int AuditId { get; set; }
        public string BusinessUnit { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Continent { get; set; }
        public string Region { get; set; }
    }
}

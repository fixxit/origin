﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LCXP.Origin.Domain.Project.ActiveDirectory
{
    [Table("project.QualificationRule")]
    public class QualificationRule
    {
        public long Id { get; set; }
        public string QualificationId { get; set; }
        public string Type { get; set; }
        public string Operator { get; set; }
        public string Object { get; set; }
        public string Condition { get; set; }
        public string Source { get; set; }
        public RuleType RuleType { get; set; }
        public string SetUserCountTo { get; set; }
        public string SetUserStateTo { get; set; }
        public string SetUserStatusTo { get; set; }

        public string SetExclusionReasonTo { get; set; }
        public Guid? BusinessUnitId { get; set; }
        public Guid ProjectId { get; set; }
        public Project Project { get; set; }

        // Device specific
        public string SetDeviceStateTo { get; set; }
        public string SetDeviceStatusTo { get; set; }
        public bool? SetOfficeEligibleDeviceTo { get; set; }
        public string SetObservationTypeTo { get; set; }
        public string SetObservationTo { get; set; }
        public string SetActionTo { get; set; }
    }
}

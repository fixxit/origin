using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LCXP.Origin.Domain.Project.ActiveDirectory
{
    public class OrganizationalUnit
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long OrganizationalUnitId { get; set; }
        public string OuId { get; set; } // "OU" + Hex value (generated)
        public string Id { get; set; } // objectGuid -> ID
        public string DistinguishedName { get; set; } // distinguishedName
        public string _OrganizationalUnit { get; set; } // dependent on objectClass === "organizationalUnit" built up using OU values separated by \\ from distinguishedName from right to left
        public string Domain { get; set; } // distinguishedNames DC values from left to right
        public string Comments { get; set; } // description
        //public BusinessUnit BusinessUnit { get; set; } // Create object
        //public int AuditId { get; set; } // investigate after classification macros were run
    }
}

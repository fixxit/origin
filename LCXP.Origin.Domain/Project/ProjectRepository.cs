﻿using System;
using System.Collections.Generic;
using System.Linq;
using LCXP.Origin.Domain.Project.ActiveDirectory;

namespace LCXP.Origin.Domain.Project
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly OriginContext _originContext;

        public ProjectRepository(OriginContext originContext)
        {
            _originContext = originContext;
        }

        public Configuration GetConfiguration(Guid projectId)
        {
            var configuration = _originContext.Configurations.SingleOrDefault(c => c.ProjectId == projectId);
            return configuration;
        }

        public List<BusinessUnit> GetBusinessUnits(Guid projectId)
        {
            var businessUnits = _originContext.Configurations
                .Where(c => c.ProjectId == projectId)
                .Select(c => c.BusinessUnits).FirstOrDefault().ToList();

            return businessUnits;
        }

        public void DeleteBusinessUnit(Guid businessUnitId)
        {
            var existingBusinessUnit = _originContext.BusinessUnits.Single(b => b.Id == businessUnitId);
            _originContext.BusinessUnits.Remove(existingBusinessUnit);
            _originContext.SaveChanges();
        }

        public Configuration SaveConfiguration(Configuration configuration)
        {
            var currentConfiguration = _originContext.Configurations.Include("BusinessUnits").Single(c => c.Id == configuration.Id);
            currentConfiguration.Parent = configuration.Parent;
            currentConfiguration.ManagedManufacturers = configuration.ManagedManufacturers;
            currentConfiguration.DateFormat = configuration.DateFormat;
            currentConfiguration.ReferenceDate = configuration.ReferenceDate;
            currentConfiguration.SnapshotDate = configuration.SnapshotDate;
            currentConfiguration.FilePath = configuration.FilePath;
            // TODO: Fix this shit

            foreach (var bu in configuration.BusinessUnits)
            {
                var existingBusinessUnit = currentConfiguration.BusinessUnits.SingleOrDefault(b => b.Id == bu.Id);
                if (existingBusinessUnit != null)
                {
                    existingBusinessUnit.Name = bu.Name;
                    existingBusinessUnit.City = bu.City;
                    existingBusinessUnit.Country = bu.Country;
                    existingBusinessUnit.Continent = bu.Continent;
                    existingBusinessUnit.Region = bu.Region;
                }
                else
                {
                    currentConfiguration.BusinessUnits.Add(bu);
                }
            }

            _originContext.SaveChanges();
            return currentConfiguration;
        }

        public List<Project> GetClientProjects(Guid clientId)
        {
            var projects = _originContext.Projects.Where(p => p.ClientId == clientId);
            return projects.ToList();
        }

        public List<ClassificationRule> GetClassificationRules(Guid projectId, RuleType ruleType)
        {
            var rules = _originContext.ClassificationRules
                .Where(r => r.ProjectId == projectId && r.RuleType == ruleType).ToList();
            return rules;
        }

        public List<QualificationRule> GetQualificationRules(Guid projectId, RuleType ruleType)
        {
            var rules = _originContext.QualificationRules
                .Where(q => q.ProjectId == projectId && q.RuleType == ruleType).ToList();
            return rules;
        }

        public ClassificationRule SaveClassificationRule(ClassificationRule rule)
        {
            var businessUnit = _originContext.BusinessUnits.Single(x => x.Id == rule.BusinessUnitId);
            if (businessUnit != null)
            {
                rule.SetBusinessUnitTo = businessUnit.Name;
                rule.SetCityTo = businessUnit.City;
                rule.SetContinentTo = businessUnit.Continent;
                rule.SetRegionTo = businessUnit.Region;
                rule.SetCountryTo = businessUnit.Country;
            }

            var existingRule = _originContext.ClassificationRules.SingleOrDefault(r => r.Id == rule.Id);
            if (existingRule != null)
            {
                existingRule.Type = rule.Type;
                existingRule.Operator = rule.Operator;
                existingRule.Object = rule.Object;
                existingRule.Source = rule.Source;
                existingRule.SetRoleTo = rule.SetRoleTo;
                existingRule.SetTypeTo = rule.SetTypeTo;
                existingRule.SetClassTo = rule.SetClassTo;
                existingRule.SetStateTo = rule.SetStateTo;
                existingRule.RuleType = rule.RuleType;
                existingRule.BusinessUnitId = rule.BusinessUnitId;
                existingRule.ClassificationTarget = rule.ClassificationTarget;
                existingRule.DeviceType = rule.DeviceType;
                existingRule.SetEnvironmentTo = rule.SetEnvironmentTo;
                existingRule.SetBusinessUnitTo = rule.SetBusinessUnitTo;
                existingRule.SetCountryTo = rule.SetCountryTo;
                existingRule.SetCityTo = rule.SetCityTo;
                existingRule.SetContinentTo = rule.SetContinentTo;
                existingRule.SetRegionTo = rule.SetRegionTo;

                _originContext.SaveChanges();
                return existingRule;
            }

            var newRule = _originContext.ClassificationRules.Add(rule);
            _originContext.SaveChanges();
            return newRule;
        }

        public void DeleteClassificationRule(long ruleId)
        {
            var existingRule = _originContext.ClassificationRules.Single(r => r.Id == ruleId);
            _originContext.ClassificationRules.Remove(existingRule);
            _originContext.SaveChanges();
        }

        public QualificationRule SaveQualificationRule(QualificationRule rule)
        {
            var existingRule = _originContext.QualificationRules.SingleOrDefault(r => r.Id == rule.Id);
            if (existingRule != null)
            {
                existingRule.QualificationId = rule.QualificationId;
                existingRule.Type = rule.Type;
                existingRule.Operator = rule.Operator;
                existingRule.Object = rule.Object;
                existingRule.Condition = rule.Condition;
                existingRule.Source = rule.Source;
                existingRule.RuleType = rule.RuleType;
                existingRule.SetUserCountTo = rule.SetUserCountTo;
                existingRule.SetUserStateTo = rule.SetUserStateTo;
                existingRule.SetUserStatusTo = rule.SetUserStatusTo;
                existingRule.SetExclusionReasonTo = rule.SetExclusionReasonTo;
                existingRule.BusinessUnitId = rule.BusinessUnitId;
                existingRule.SetDeviceStateTo = rule.SetDeviceStateTo;
                existingRule.SetDeviceStatusTo = rule.SetDeviceStatusTo;
                existingRule.SetOfficeEligibleDeviceTo = rule.SetOfficeEligibleDeviceTo;
                existingRule.SetObservationTypeTo = rule.SetObservationTypeTo;
                existingRule.SetObservationTo = rule.SetObservationTo;
                existingRule.SetActionTo = rule.SetActionTo;
                _originContext.SaveChanges();
                return existingRule;
            }

            var newRule = _originContext.QualificationRules.Add(rule);
            _originContext.SaveChanges();
            return newRule;
        }

        public void DeleteQualificationRule(long ruleId)
        {
            var existingRule = _originContext.QualificationRules.Single(r => r.Id == ruleId);
            _originContext.QualificationRules.Remove(existingRule);
            _originContext.SaveChanges();
        }
    }

    class BusinessUnitComparer : IEqualityComparer<BusinessUnit>
    {
        public bool Equals(BusinessUnit x, BusinessUnit y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(BusinessUnit obj)
        {
            return obj.GetHashCode();
        }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LCXP.Origin.Domain.Project
{
    [Table("project.BusinessUnit")]
    public class BusinessUnit
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Continent { get; set; }
        public string Region { get; set; }
        public Guid ConfigurationId { get; set; }
        public Configuration Configuration { get; set; }
    }
}

﻿using LCXP.Origin.Domain.Project.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace LCXP.Origin.Domain.Project
{
    [Table("project.Project")]
    public class Project
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CommencementDate { get; set; }
        public Guid ClientId { get; set; }
        public Client.Client Client { get; set; }
        public Configuration Configuration { get; set; }
        public ICollection<ClassificationRule> ClassificationRules { get; set; }
        public ICollection<QualificationRule> QualificationRules { get; set; }
        //public ActiveDirectoryGroup ActiveDirectoryGroup { get; set; }
        //public OrganizationalUnit OrganizationalUnit { get; set; }
        //public ActiveDirectory.User.Inventory UserInventory { get; set; }
        //public ActiveDirectory.Device.Inventory DeviceInventory { get; set; }
    }
}

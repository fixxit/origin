﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace LCXP.Origin.Domain.Project
{
    [Table("project.Configuration")]
    public class Configuration
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Parent { get; set; }
        public string ManagedManufacturers { get; set; }
        public string DateFormat { get; set; }
        public DateTime? ReferenceDate { get; set; }
        public DateTime? SnapshotDate { get; set; }
        public string FilePath { get; set; }
        public ICollection<BusinessUnit> BusinessUnits { get; set; }
        public Guid ProjectId { get; set; }
        public Project Project { get; set; }
    }
}

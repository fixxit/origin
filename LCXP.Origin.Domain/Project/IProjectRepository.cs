﻿using LCXP.Origin.Domain.Project.ActiveDirectory;
using System;
using System.Collections.Generic;

namespace LCXP.Origin.Domain.Project
{
    public interface IProjectRepository
    {
        List<Project> GetClientProjects(Guid clientId);
        Configuration SaveConfiguration(Configuration configuration);
        Configuration GetConfiguration(Guid projectId);
        List<BusinessUnit> GetBusinessUnits(Guid projectId);
        void DeleteBusinessUnit(Guid businessUnitId);
        List<ClassificationRule> GetClassificationRules(Guid projectId, RuleType ruleType);
        List<QualificationRule> GetQualificationRules(Guid projectId, RuleType ruleType);
        ClassificationRule SaveClassificationRule(ClassificationRule rule);
        void DeleteClassificationRule(long ruleId);
        QualificationRule SaveQualificationRule(QualificationRule rule);
        void DeleteQualificationRule(long ruleId);
    }
}

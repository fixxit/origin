﻿namespace LCXP.Origin.Domain.FileHelper
{
    public class CsvConfig
    {
        public char Delimiter { get; private set; }
        public string NewLineMark { get; private set; }
        public char QuotationMark { get; private set; }

        public CsvConfig(char delimiter, string newLineMark, char quotationMark)
        {
            Delimiter = delimiter;
            NewLineMark = newLineMark;
            QuotationMark = quotationMark;
        }
        public static CsvConfig Default
        {
            get { return new CsvConfig(',', "\r\n", '\"'); }
        }
    }
}

﻿using LCXP.Origin.Domain.Project.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LCXP.Origin.Domain.ActiveDirectory
{
    public class ImportUsers
    {
        private readonly OriginContext _originContext;
        private ADHelpers _adHelper { get; set; }
        private string _file { get; set; }
        public ImportUsers(OriginContext originContext, string fileName)
        {
            _originContext = originContext;
            _adHelper = new ADHelpers(fileName);
            _file = fileName;
        }

        public void ImportUserInventory()
        {
            _originContext.Database.ExecuteSqlCommand("TRUNCATE TABLE UserInventory");
            var listUsers = GetUserInventory();

            var verifyUsers = new VerifyUsers(_originContext, listUsers, _file);

            var allVerifiedUsers = verifyUsers.VerifyAllUsers();

            foreach (var userInventory in allVerifiedUsers)
            {
                _originContext.UserInventory.Add(userInventory);
                //db.SaveChanges();//per record
            }
            _originContext.SaveChanges();//bulk
        }

        private List<UserInventory> GetUserInventory()
        {
            var listUserInventory = new List<UserInventory>();

            var listKVP = _adHelper.ImportIntoKVPList();
            var userList = _adHelper.GetTypeList(listKVP, "user");
            //todo: can clear/dispose listKVP at this point

            var counter = 0;
            foreach (var userKVPs in userList)
            {
                counter++;
                var userInventory = new UserInventory();

                #region 1. UserId + 12. ParentID + 20. CollectionDate
                userInventory.UserId = _adHelper.GetHexId("U",counter);
                userInventory.ParentID = userInventory.UserId;
                // todo: need to come back and do error handeling
                userInventory.CollectionDate = _adHelper.GetDateFromFileName();
                #endregion

                #region 16. LastActivityDate
                // lastLogonTimestamp
                //todo: can make better and quicker
                var dtLastActivityDate = new DateTime();

                var lastLogon = userKVPs.Where(key => key.Key == "lastLogon").Select(key => key.Value).ToList();
                var lastLogonTimestamp = userKVPs.Where(key => key.Key == "lastLogonTimestamp").Select(key => key.Value).ToList();
                var whenCreated = userKVPs.Where(key => key.Key == "whenCreated").Select(key => key.Value).ToList();
                var pwdLastSet = userKVPs.Where(key => key.Key == "pwdLastSet").Select(key => key.Value).ToList();
                var created = whenCreated[0].Substring(0, whenCreated[0].IndexOf('.'));
                userInventory.WhenCreatedDate = new DateTime(Int16.Parse(created.Substring(0, 4)), Int16.Parse(created.Substring(4, 2)), Int16.Parse(created.Substring(6, 2)));

                if (!String.IsNullOrEmpty(lastLogon[0].Trim()) && !String.IsNullOrEmpty(lastLogonTimestamp[0].Trim()) && !String.IsNullOrEmpty(pwdLastSet[0].Trim()))
                {
                    var lastLogonTS = DateTime.FromFileTimeUtc(Int64.Parse(lastLogon[0]));
                    var pwdLastSetTS = DateTime.FromFileTimeUtc(Int64.Parse(pwdLastSet[0]));
                    var lastLogonTimeSTanp = DateTime.FromFileTimeUtc(Int64.Parse(lastLogonTimestamp[0]));

                    if (lastLogonTS > lastLogonTimeSTanp)
                    {
                        dtLastActivityDate = lastLogonTS;
                    }
                    else
                    {
                        dtLastActivityDate = lastLogonTimeSTanp;
                    }
                    if (pwdLastSetTS > lastLogonTimeSTanp)
                    {
                        dtLastActivityDate = pwdLastSetTS;
                    }
                    else
                    {
                        dtLastActivityDate = lastLogonTimeSTanp;
                    }
                    if (lastLogonTS > dtLastActivityDate)
                    {
                        dtLastActivityDate = lastLogonTS;
                    }
                    userInventory.LastActivityDate = dtLastActivityDate;
                }
                else if (!String.IsNullOrEmpty(lastLogonTimestamp[0].Trim()) && !String.IsNullOrEmpty(pwdLastSet[0].Trim()))
                {
                    var pwdLastSetTS = DateTime.FromFileTimeUtc(Int64.Parse(pwdLastSet[0]));
                    var lastLogonTimeSTanp = DateTime.FromFileTimeUtc(Int64.Parse(lastLogonTimestamp[0]));
                    if (pwdLastSetTS > lastLogonTimeSTanp)
                    {
                        dtLastActivityDate = pwdLastSetTS;
                    }
                    else
                    {
                        dtLastActivityDate = lastLogonTimeSTanp;
                    }
                    userInventory.LastActivityDate = dtLastActivityDate;
                }
                else if (!String.IsNullOrEmpty(lastLogonTimestamp[0].Trim()))
                {
                    userInventory.LastActivityDate = DateTime.FromFileTimeUtc(Int64.Parse(lastLogonTimestamp[0]));
                }
                else if (!String.IsNullOrEmpty(pwdLastSet[0].Trim()))
                {
                    if (pwdLastSet[0].Length > 2)
                    {
                        userInventory.LastActivityDate = DateTime.FromFileTimeUtc(Int64.Parse(pwdLastSet[0]));
                    }
                }

                #endregion
                #region 17. DaysSinceLastActivity
                // date diff between last activity date and creation time
                var dateCollection = new DateTime(userInventory.CollectionDate.Year, userInventory.CollectionDate.Month, userInventory.CollectionDate.Day);

                var dateLastActive = new DateTime();
                if (userInventory.LastActivityDate.HasValue)
                {
                    var lastActivityDate = userInventory.LastActivityDate.Value;
                    dateLastActive = new DateTime(lastActivityDate.Year, lastActivityDate.Month, lastActivityDate.Day);
                    userInventory.DaysSinceLastActivity = dateCollection.Subtract(dateLastActive).Days;
                }
                else
                {
                    userInventory.DaysSinceLastActivity = null;
                }

                #endregion

                #region 18. AgeOfRecord
                // date diff between collection date and creation date (collection date is in file name)
                var createdD = whenCreated[0].Substring(0, whenCreated[0].IndexOf('.'));
                var createdDate = new DateTime(Int16.Parse(createdD.Substring(0, 4)), Int16.Parse(createdD.Substring(4, 2)), Int16.Parse(createdD.Substring(6, 2)));
                userInventory.AgeOfRecord = userInventory.CollectionDate.Subtract(createdDate).Days;
                #endregion


                foreach (var kvp in userKVPs)
                {
                    //todo: dont believe foreach is needed. remove out of and point to exact kvp's
                    #region 2. UserName + 4. PrincipalName
                    if (kvp.Key == "sAMAccountName")
                    {
                        if (!String.IsNullOrEmpty(kvp.Value))
                        {
                            userInventory.UserName = kvp.Value;
                        }
                    }
                    if (kvp.Key == "userPrincipalName")
                    {
                        if (!String.IsNullOrEmpty(kvp.Value))
                        {
                            userInventory.PrincipalName = kvp.Value;
                        }
                    }
                    #endregion
                    #region 3. DisplayName + 5. Domain + 6. OrganizationalUnit + 7. DistinguishedName
                    // display name is first CN of distinguishedName
                    if (kvp.Key == "distinguishedName")
                    {
                        if (!String.IsNullOrEmpty(kvp.Value))
                        {
                            if (kvp.Value.Contains("X'"))
                            {
                                //var hextToString = _adHelper.HexStringToBytes(kvp.Value.TrimEnd('\'').Replace("X'",""));
                                //userInventory.DistinguishedName = Encoding.GetEncoding("ISO-8859-1").GetString(hextToString);
                                userInventory.DistinguishedName = _adHelper.HexStringToString(kvp.Value);
                            }
                            else
                            {
                                userInventory.DistinguishedName = kvp.Value;
                            }
                            var firstCn = Array.Find(userInventory.DistinguishedName.Split(','), element => element.ToLower().StartsWith("cn"));
                            if (!string.IsNullOrEmpty(firstCn))
                            {
                                userInventory.DisplayName = firstCn.Substring(firstCn.IndexOf('=') + 1);
                            }
                            var firstDc = Array.Find(userInventory.DistinguishedName.Split(','), element => element.ToLower().StartsWith("dc"));
                            if (!string.IsNullOrEmpty(firstDc))
                            {
                                userInventory.Domain = firstDc.Substring(firstDc.IndexOf('=') + 1).ToUpper();
                            }

                            var allOU = Array.FindAll(userInventory.DistinguishedName.Split(','), element => element.ToLower().StartsWith("ou"));
                            if (allOU.Length > 0)
                            {
                                userInventory.OrganizationalUnit = "\\\\";
                                for (int i = allOU.Length - 1; i >= 0; i--)
                                {
                                    userInventory.OrganizationalUnit += allOU[i].Substring(allOU[i].IndexOf('=') + 1) + "\\";
                                }
                            }
                        }
                    }
                    #endregion
                    #region 8. ADname + 10. Last Name + 11. MaidenName
                    // 8 - sn
                    // 10 - ADname formatted with each word starting with capital letter
                    // 11 - ADName split if contains a dash, last name is taken
                    if (kvp.Key == "sn")
                    {
                        if (!String.IsNullOrEmpty(kvp.Value))
                        {
                            userInventory.ADname = kvp.Value;
                            userInventory.LastName = kvp.Value;//todo comeback and add format

                            if (kvp.Value.Contains('-'))
                            {
                                userInventory.MaidenName = kvp.Value.Substring(kvp.Value.IndexOf('-')).Trim();
                            }
                        }
                    }

                    #endregion
                    #region 9. FirstName
                    // givenName
                    if (kvp.Key == "givenName")
                    {
                        if (!String.IsNullOrEmpty(kvp.Value))
                        {
                            userInventory.FirstName = kvp.Value;
                        }
                    }
                    #endregion
                    #region 13. NormalizedFullName + 14. AnonymizedName
                    // 13 - FirstName + LastName
                    // 14 - Normalizedfullname with first and last 5 characters visible
                    userInventory.NormalizedFullName = userInventory.FirstName + " " + userInventory.LastName;//check/add error handeling

                    if (!String.IsNullOrEmpty(userInventory.NormalizedFullName.Trim()))
                    {
                        var lenghtName = userInventory.NormalizedFullName.Length;
                        userInventory.AnonymizedName = userInventory.NormalizedFullName.Substring(0, 1);

                        if (lenghtName - 6 > 0)
                        {
                            for (int i = 0; i < lenghtName - 5; i++)
                            {
                                userInventory.AnonymizedName += "*";
                            }
                            userInventory.AnonymizedName += userInventory.NormalizedFullName.Substring(lenghtName - 5);
                        }
                        else
                        {
                            for (int i = 0; i < lenghtName - 1; i++)
                            {
                                userInventory.AnonymizedName += "*";
                            }
                        }
                    }
                    #endregion
                    #region 15. MailboxAddress
                    // mail
                    if (kvp.Key == "mail")
                    {
                        if (!String.IsNullOrEmpty(kvp.Value))
                        {
                            userInventory.MailboxAddress = kvp.Value;
                        }
                    }
                    #endregion
                    #region 19. Disabled
                    // ask when results in this field being true
                    //todo: create own function as might be used else where
                    if (kvp.Key == "userAccountControl")
                    {

                        if (!String.IsNullOrEmpty(kvp.Value))
                        {
                            const int UF_ACCOUNTDISABLE = 0x0002;

                            int flags = Convert.ToInt32(kvp.Value);

                            if (Convert.ToBoolean(flags & UF_ACCOUNTDISABLE))
                            {
                                userInventory.Disabled = true;
                            }
                        }
                    }

                    #endregion
                    #region 21. Metric
                    // is this still used?

                    #endregion
                    #region 22. UserRole
                    // type (maybe msExchDeviceType) // classification rules

                    #endregion
                    #region 23. UserType
                    // classification rules

                    #endregion
                    #region 24. UserClass
                    // classification rules

                    #endregion
                    #region 25. UserState
                    // classification rules

                    #endregion
                    #region 26. UserStatus
                    // substate (user exceptions)

                    #endregion
                    #region 27. Funnel
                    // legacy?

                    #endregion
                    #region 28. ClassificationRules
                    // configured rules

                    #endregion
                    #region 29. ExceptionRules
                    // configured exceptions

                    #endregion
                    #region 30. UserCount
                    // configured exceptions

                    #endregion
                    #region 31. Comments
                    // description
                    if (kvp.Key == "description")
                    {
                        if (!String.IsNullOrEmpty(kvp.Value))
                        {
                            userInventory.Comments = kvp.Value;
                        }
                    }

                    #endregion
                    #region 32. Observations
                    //check looks like in rules
                    //todo:ask about this one

                    #endregion
                }
                listUserInventory.Add(userInventory);
            }
            return listUserInventory;
        }

    }
}
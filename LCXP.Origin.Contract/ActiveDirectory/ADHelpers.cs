﻿using LCXP.Origin.Domain.FileHelper;
using LCXP.Origin.Domain.Project.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LCXP.Origin.Domain.ActiveDirectory
{
    public class ADHelpers
    {
        private string _file { get; set; }
        public ADHelpers(string fileName)
        {
            _file = fileName;
        }

        public string GetHexId(string prefix, int record)
        {
            return prefix + record.ToString("X4");
        }
        public DateTime GetDateFromFileName()
        {
            var returnDate = new DateTime();
            try
            {
                var fileDate = _file.Substring(_file.LastIndexOf('_') + 1);
                fileDate = fileDate.Substring(0, fileDate.IndexOf('.'));

                var fileYear = int.Parse(fileDate.Substring(0, 4));
                var fileMonth = int.Parse(fileDate.Substring(4, 2));
                var fileDay = int.Parse(fileDate.Substring(6, 2));

                returnDate = new DateTime(fileYear, fileMonth, fileDay);
            }
            catch (Exception e)
            {
                //todo: error handeling
                throw;
            }
            return returnDate;
        }

        public List<List<KeyValuePair<string, string>>> ImportIntoKVPList()
        {
            var returnList = new List<List<KeyValuePair<string, string>>>() { };
            var listADRecords = new List<string[]>();

            var reader = new CsvReader();
            string csv = System.IO.File.ReadAllText(_file);
            foreach (var row in reader.Read(csv))
            {
                var rowData = new List<string>() { };

                foreach (var cell in row)
                {
                    rowData.Add(cell);
                }
                listADRecords.Add(rowData.ToArray<string>());
            }


            var headers = listADRecords[0];
            var countRow = 0;
            foreach (var record in listADRecords)
            {
                var listKVP = new List<KeyValuePair<string, string>>() { };
                if (countRow > 0)
                {
                    for (int i = 0; i < record.Length; i++)
                    {
                        var kvp = new KeyValuePair<string, string>(headers[i].ToString(), record[i].ToString());
                        listKVP.Add(kvp);
                    }
                    returnList.Add(listKVP);
                }
                countRow++;
            }
            return returnList;
        }

        public List<List<KeyValuePair<string, string>>> GetTypeList(List<List<KeyValuePair<string, string>>> listKVP, string listOf)
        {//todo: could extract to get list of user,computer.....
            var userList = new List<List<KeyValuePair<string, string>>>();
            try
            {
                foreach (var kvps in listKVP)
                {

                    foreach (var kvp in kvps)
                    {
                        if (kvp.Key == "objectClass")
                        {
                            if (kvp.Value.ToLower() == listOf)
                            {
                                userList.Add(kvps);
                            }
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //todo: error handeling
                throw;
            }
            return userList;
        }

        public List<List<QualificationRule>> GetComboQualifications(List<QualificationRule> listQualifications) {
            
            var comboListQualifications = new List<List<QualificationRule>>();

            var newMultiList = new List<QualificationRule>();

            var previousId = "";
            foreach (var userException in listQualifications)
            {
                if (String.IsNullOrEmpty(previousId))
                {
                    previousId = userException.QualificationId;
                }

                if (previousId == userException.QualificationId)
                {
                    newMultiList.Add(userException);
                }
                else
                {
                    previousId = userException.QualificationId;
                    comboListQualifications.Add(newMultiList);
                    newMultiList = new List<QualificationRule>();
                    newMultiList.Add(userException);
                }
            }
            if (newMultiList.Count != 0)
            {
                comboListQualifications.Add(newMultiList);
                newMultiList = new List<QualificationRule>();
            }

            return comboListQualifications;
        }

        public string HexStringToString(string hexString)
        {
            if (hexString.StartsWith("X'"))
            {
                hexString = hexString.TrimEnd('\'').Replace("X'", "");
            }
            if (hexString == null)
                throw new ArgumentNullException("hexString");
            if (hexString.Length % 2 != 0)
                throw new ArgumentException("hexString must have an even length", "hexString");
            var bytes = new byte[hexString.Length / 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                string currentHex = hexString.Substring(i * 2, 2);
                bytes[i] = Convert.ToByte(currentHex, 16);
            }
            return Encoding.GetEncoding("ISO-8859-1").GetString(bytes);
        }

        public DateTime GetLastActiveDate()
        {
            var returnDateTime = new DateTime();
            return returnDateTime;
        }
    }
}

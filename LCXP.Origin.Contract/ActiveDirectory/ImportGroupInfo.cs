﻿using LCXP.Origin.Domain.Project.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LCXP.Origin.Domain.ActiveDirectory
{
    public class ImportGroupInfo
    {
        private ADHelpers _adHelper { get; set; }
        public ImportGroupInfo(string fileName)
        {
            _adHelper = new ADHelpers(fileName);
        }
        public List<ActiveDirectoryGroup> GetGroupInfo()
        {
            var listAdGroup = new List<ActiveDirectoryGroup>();

            var listKVP = _adHelper.ImportIntoKVPList();
            var groupList = _adHelper.GetTypeList(listKVP, "group");

            var counter = 0;
            foreach (var gpKVPs in groupList)
            {
                counter++;
                var adGroup = new ActiveDirectoryGroup();

                adGroup.ContainerId = "CN" + counter.ToString("X4");

                var distinguishedName = gpKVPs.Where(key => key.Key == "distinguishedName").Select(key => key.Value).ToList().First();
                var adGroupName = "";
                if (distinguishedName.Contains(','))
                {
                    adGroupName = Array.Find(distinguishedName.Split(','), element => element.ToLower().StartsWith("cn")).ToLower().Replace("cn=","");
                }
                adGroup.AdGroupName = adGroupName;
                adGroup.DistinguishedName = distinguishedName;
                if (distinguishedName.Contains(','))
                {
                    var allOU = Array.FindAll(distinguishedName.Split(','), element => element.ToLower().StartsWith("ou"));
                    var ou = "\\\\";
                    if (allOU.Length > 0)
                    {
                        for (int i = allOU.Length - 1; i >= 0; i--)
                        {
                            ou += allOU[i].Substring(allOU[i].IndexOf('=') + 1) + "\\";
                        }
                    }
                    adGroup.OrganizationalUnit = ou;
                }
                if (distinguishedName.Contains(','))
                {
                    var allDC = Array.FindAll(distinguishedName.Split(','), element => element.ToLower().StartsWith("dc"));
                    var domain = "";
                    if (allDC.Length > 0)
                    {
                        for (int i = 0; i <= allDC.Length - 1; i++)
                        {
                            domain += allDC[i].Substring(allDC[i].IndexOf('=') + 1) + ".";
                        }
                    }
                    adGroup.Domain = domain.TrimEnd('.');
                }
                var setMSEligible = false;

                if (adGroupName.Contains("microsoft") || adGroupName.Contains("access") || adGroupName.Contains("excel") || adGroupName.Contains("onenote"))
                {
                    setMSEligible = true;
                }
                if (adGroupName.Contains("outlook") || adGroupName.Contains("powerpoint") || adGroupName.Contains("publisher"))
                {
                    setMSEligible = true;
                }
                if (adGroupName.Contains("word") || adGroupName.Contains("ms ") || adGroupName.Contains("office") || adGroupName.Contains("visio"))
                {
                    setMSEligible = true;
                }
                adGroup.MsEligibleGroup = setMSEligible;

                var comments = gpKVPs.Where(key => key.Key == "description").Select(key => key.Value).ToList().First();
                adGroup.Comments = comments;

                listAdGroup.Add(adGroup);
            }

            return listAdGroup;
        }
    }
}

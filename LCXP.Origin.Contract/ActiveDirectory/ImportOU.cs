﻿using LCXP.Origin.Domain.Project.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LCXP.Origin.Domain.ActiveDirectory
{
    public class ImportOU
    {
        private ADHelpers _adHelper { get; set; }
        public ImportOU(string fileName)
        {
            _adHelper = new ADHelpers(fileName);
        }

        public List<OrganizationalUnit> GetOUs()
        {
            var listOrganizationalUnit = new List<OrganizationalUnit>();

            var listKVP = _adHelper.ImportIntoKVPList();
            var ouList = _adHelper.GetTypeList(listKVP, "organizationalunit");

            var counter = 0;
            foreach (var ouKVPs in ouList)
            {
                counter++;
                var organizationalUnit = new OrganizationalUnit();

                #region 1. UserId + 2. Id
                organizationalUnit.OuId = "OU" + counter.ToString("X4");

                var objectGuid = ouKVPs.Where(key => key.Key.ToLower() == "objectguid").Select(key => key.Value).ToList().First();
                if (objectGuid.Contains("X'"))
                {
                    organizationalUnit.Id = objectGuid.Replace("X'","").TrimEnd('\'');
                }

                var distinguishedName = ouKVPs.Where(key => key.Key == "distinguishedName").Select(key => key.Value).ToList().First();
                organizationalUnit.DistinguishedName = distinguishedName;


                var allOU = Array.FindAll(distinguishedName.Split(','), element => element.ToLower().StartsWith("ou"));
                var ou = "\\\\";
                if (allOU.Length > 0)
                {
                    for (int i = allOU.Length - 1; i >= 0; i--)
                    {
                        ou += allOU[i].Substring(allOU[i].IndexOf('=') + 1) + "\\";
                    }
                }
                organizationalUnit._OrganizationalUnit = ou;


                var allDC = Array.FindAll(distinguishedName.Split(','), element => element.ToLower().StartsWith("dc"));
                var domain = "";
                if (allDC.Length > 0)
                {
                    for (int i = 0; i <= allDC.Length - 1; i++)
                    {
                        domain += allDC[i].Substring(allDC[i].IndexOf('=') + 1) + ".";
                    }
                }
                organizationalUnit.Domain = domain.TrimEnd('.');
                // todo: need to come back and do error handeling
                #endregion

                listOrganizationalUnit.Add(organizationalUnit);
            }

            return listOrganizationalUnit;
        }
    }
}

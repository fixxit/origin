﻿using LCXP.Origin.Contract.ActiveDirectory;
using LCXP.Origin.Domain.Project.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LCXP.Origin.Domain.ActiveDirectory
{
    public class ImportDevices
    {
        private readonly OriginContext _originContext;
        private ADHelpers _adHelper { get; set; }
        private string _file { get; set; }

        public ImportDevices(OriginContext originContext, string fileName)
        {
            _originContext = originContext;
            _adHelper = new ADHelpers(fileName);
            _file = fileName;
        }

        public void ImportDeviceInventory() {
            //TODO: clean table out
            _originContext.Database.ExecuteSqlCommand("TRUNCATE TABLE [dbo].[DeviceInventory]");

            var listDevices = GetDeviceInventory();
            var verifyDevices = new VerifyDevices(_originContext, listDevices, _file);

            var allVerifiedDevices = verifyDevices.VerifyAllDevices();

            foreach (var deviceInventory in allVerifiedDevices)
            {
                _originContext.DeviceInventory.Add(deviceInventory);
                //db.SaveChanges();//per record
            }
            _originContext.SaveChanges();//bulk
        }
        private List<DeviceInventory> GetDeviceInventory()
        {
            var listDeviceInventory = new List<DeviceInventory>();

            var listKVP = _adHelper.ImportIntoKVPList();
            var deviceList = _adHelper.GetTypeList(listKVP, "computer");

            var osLookup = _originContext.OSConversions.ToList();

            var counter = 0;
            foreach (var deviceKVPs in deviceList)
            {
                counter++;
                var deviceInventory = new DeviceInventory();
                deviceInventory.DeviceId = _adHelper.GetHexId("D", counter);
                deviceInventory.CollectionDate = _adHelper.GetDateFromFileName();

                var distinguishedName = deviceKVPs.Where(key => key.Key == "distinguishedName").Select(key => key.Value).ToList().First();

                var deviceName = deviceKVPs.Where(key => key.Key == "name").Select(key => key.Value).ToList().First();
                if (String.IsNullOrEmpty(deviceName))
                {
                    deviceName = deviceKVPs.Where(key => key.Key == "cn").Select(key => key.Value).ToList().First();
                }
                deviceInventory.DeviceName = deviceName;

                if (distinguishedName.Contains(','))
                {
                    var allDC = Array.FindAll(distinguishedName.Split(','), element => element.ToLower().StartsWith("dc"));
                    var domain = "";
                    if (allDC.Length > 0)
                    {
                        for (int i = 0; i <= allDC.Length - 1; i++)
                        {
                            domain += allDC[i].Substring(allDC[i].IndexOf('=') + 1) + ".";
                        }
                    }
                    deviceInventory.FullyQualifiedDomainName = deviceName + "." + domain.TrimEnd('.');
                    deviceInventory.Domain = domain.TrimEnd('.');
                }
                var allOU = Array.FindAll(distinguishedName.Split(','), element => element.ToLower().StartsWith("ou"));
                var ou = "\\\\";
                if (allOU.Length > 0)
                {
                    for (int i = allOU.Length - 1; i >= 0; i--)
                    {
                        ou += allOU[i].Substring(allOU[i].IndexOf('=') + 1) + "\\";
                    }
                }
                deviceInventory.OrganizationalUnit = ou;

                #region LastActivityDate
                // lastLogonTimestamp
                //todo: can make better and quicker
                var dtLastActivityDate = new DateTime();

                var lastLogon = deviceKVPs.Where(key => key.Key == "lastLogon").Select(key => key.Value).ToList();
                var lastLogonTimestamp = deviceKVPs.Where(key => key.Key == "lastLogonTimestamp").Select(key => key.Value).ToList();
                var whenCreated = deviceKVPs.Where(key => key.Key == "whenCreated").Select(key => key.Value).ToList();
                var pwdLastSet = deviceKVPs.Where(key => key.Key == "pwdLastSet").Select(key => key.Value).ToList();
                var created = whenCreated[0].Substring(0, whenCreated[0].IndexOf('.'));
                deviceInventory.WhenCreatedDate = new DateTime(Int16.Parse(created.Substring(0, 4)), Int16.Parse(created.Substring(4, 2)), Int16.Parse(created.Substring(6, 2)));

                if (!String.IsNullOrEmpty(lastLogon[0].Trim()) && !String.IsNullOrEmpty(lastLogonTimestamp[0].Trim()) && !String.IsNullOrEmpty(pwdLastSet[0].Trim()))
                {
                    var lastLogonTS = DateTime.FromFileTimeUtc(Int64.Parse(lastLogon[0]));
                    var pwdLastSetTS = DateTime.FromFileTimeUtc(Int64.Parse(pwdLastSet[0]));
                    var lastLogonTimeSTanp = DateTime.FromFileTimeUtc(Int64.Parse(lastLogonTimestamp[0]));

                    if (lastLogonTS > lastLogonTimeSTanp)
                    {
                        dtLastActivityDate = lastLogonTS;
                    }
                    else
                    {
                        dtLastActivityDate = lastLogonTimeSTanp;
                    }
                    if (pwdLastSetTS > lastLogonTimeSTanp)
                    {
                        dtLastActivityDate = pwdLastSetTS;
                    }
                    else
                    {
                        dtLastActivityDate = lastLogonTimeSTanp;
                    }
                    if (lastLogonTS > dtLastActivityDate)
                    {
                        dtLastActivityDate = lastLogonTS;
                    }
                    deviceInventory.LastActivityDate = dtLastActivityDate;
                }
                else if (!String.IsNullOrEmpty(lastLogonTimestamp[0].Trim()) && !String.IsNullOrEmpty(pwdLastSet[0].Trim()))
                {
                    var pwdLastSetTS = DateTime.FromFileTimeUtc(Int64.Parse(pwdLastSet[0]));
                    var lastLogonTimeSTanp = DateTime.FromFileTimeUtc(Int64.Parse(lastLogonTimestamp[0]));
                    if (pwdLastSetTS > lastLogonTimeSTanp)
                    {
                        dtLastActivityDate = pwdLastSetTS;
                    }
                    else
                    {
                        dtLastActivityDate = lastLogonTimeSTanp;
                    }
                    deviceInventory.LastActivityDate = dtLastActivityDate;
                }
                else if (!String.IsNullOrEmpty(lastLogonTimestamp[0].Trim()))
                {
                    deviceInventory.LastActivityDate = DateTime.FromFileTimeUtc(Int64.Parse(lastLogonTimestamp[0]));
                }
                else if (!String.IsNullOrEmpty(pwdLastSet[0].Trim()))
                {
                    if (pwdLastSet[0].Length > 2)
                    {
                        deviceInventory.LastActivityDate = DateTime.FromFileTimeUtc(Int64.Parse(pwdLastSet[0]));
                    }
                }

                #endregion
                #region DaysSinceLastActivity
                // date diff between last activity date and creation time
                var dateCollection = new DateTime(deviceInventory.CollectionDate.Year, deviceInventory.CollectionDate.Month, deviceInventory.CollectionDate.Day);

                var dateLastActive = new DateTime();
                if (deviceInventory.LastActivityDate.HasValue)
                {
                    var lastActivityDate = deviceInventory.LastActivityDate.Value;
                    dateLastActive = new DateTime(lastActivityDate.Year, lastActivityDate.Month, lastActivityDate.Day);
                    deviceInventory.DaysSinceLastActivity = dateCollection.Subtract(dateLastActive).Days;
                }
                else
                {
                    deviceInventory.DaysSinceLastActivity = null;
                }

                #endregion

                #region AgeOfRecord
                // date diff between collection date and creation date (collection date is in file name)
                var createdD = whenCreated[0].Substring(0, whenCreated[0].IndexOf('.'));
                var createdDate = new DateTime(Int16.Parse(createdD.Substring(0, 4)), Int16.Parse(createdD.Substring(4, 2)), Int16.Parse(createdD.Substring(6, 2)));
                deviceInventory.AgeOfRecord = deviceInventory.CollectionDate.Subtract(createdDate).Days;
                #endregion

                
                var operatingSystem = deviceKVPs.Where(key => key.Key == "operatingSystem").Select(key => key.Value).ToList().First();
                if (operatingSystem.StartsWith("X'"))
                {
                    //operatingSystem = _adHelper.HexStringToString(operatingSystem);
                    //TODO:generates special chars that is not matched
                }
                if (osLookup.Exists(x => x.OSID == operatingSystem))
                {
                    var osConversion = osLookup.First(x => x.OSID == operatingSystem);
                    deviceInventory.OperatingSystem = osConversion.Product;
                    deviceInventory.Manufacturer = osConversion.Manufacturer == null ? "unspecified" : osConversion.Manufacturer;
                    deviceInventory.Version = osConversion.ProductVersion;
                    deviceInventory.DeviceRole = osConversion.Type == null ? "unspecified" : osConversion.Type;
                }
                else
                {
                    deviceInventory.Manufacturer = "unspecified";
                    deviceInventory.DeviceRole = "unspecified";
                }

                //todo:ask if this is correct
                deviceInventory.DeviceType = "unspecified";
                var userAccontControl = deviceKVPs.Where(key => key.Key == "userAccountControl").Select(key => key.Value).ToList().First();
                if (!String.IsNullOrEmpty(userAccontControl))
                {
                    const int UF_ACCOUNTDISABLE = 0x0002;

                    int flags = Convert.ToInt32(userAccontControl);

                    if (Convert.ToBoolean(flags & UF_ACCOUNTDISABLE))
                    {
                        deviceInventory.Disabled = true;
                    }
                    else {
                        deviceInventory.Disabled = false;
                    }
                }


                var comments = deviceKVPs.Where(key => key.Key == "description").Select(key => key.Value).ToList().First();
                deviceInventory.Comments = comments;



                listDeviceInventory.Add(deviceInventory);
            }
            return listDeviceInventory;
        }
    }
}

﻿using LCXP.Origin.Domain.Project.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LCXP.Origin.Domain.ActiveDirectory
{
    public class VerifyUsers
    {
        private ADHelpers _adHelper { get; set; }

        private readonly OriginContext _originContext;
        private List<UserInventory> _userInventories { get; set; }

        public VerifyUsers(OriginContext originContext, List<UserInventory> userInventories, string fileName)
        {
            _originContext = originContext;
            _userInventories = userInventories;
            _adHelper = new ADHelpers(fileName);
        }

        
        public List<UserInventory> VerifyAllUsers()
        {
            var listUserExceptions = new List<QualificationRule>();
            var listUserClassifications = new List<ClassificationRule>();

            listUserExceptions = _originContext.QualificationRules.Where(x=>x.RuleType == RuleType.User).ToList();
            listUserClassifications = _originContext.ClassificationRules.Where(x => x.RuleType == RuleType.User).ToList();

            _userInventories = ExecuteUserClassifications(_userInventories, listUserClassifications);
            _userInventories = ExecuteUserExceptions(_userInventories, listUserExceptions);

            return _userInventories;
        }

        private List<UserInventory> ExecuteUserClassifications(List<UserInventory> userInventories, List<ClassificationRule> listUserClassifications)
        {
            foreach (var userInventory in userInventories)
            {
                var classificationRules = "";
                foreach (var userClasification in listUserClassifications)
                {
                    var prop = userClasification.Type.Replace(" ", "");
                    var prop2 = userInventory.GetType().GetProperty(prop).GetValue(userInventory);

                    if (userClasification.Operator.Replace(" ", "").ToLower() == "unlike")
                    {
                        classificationRules += userClasification.ClassificationId + " (USr/" + userClasification.Type + "), ";//TODO: this might need to look at if the 1st rule
                        if (prop2 == null)
                        {
                            userInventory.UserRole = !String.IsNullOrEmpty(userClasification.SetRoleTo) ? userClasification.SetRoleTo : userInventory.UserRole;
                            userInventory.UserType = !String.IsNullOrEmpty(userClasification.SetTypeTo) ? userClasification.SetTypeTo : userInventory.UserType;
                            userInventory.UserClass = !String.IsNullOrEmpty(userClasification.SetClassTo) ? userClasification.SetClassTo : userInventory.UserClass;
                            if (userInventory.UserState == null)
                            {
                                userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) ? userClasification.SetStateTo : userInventory.UserState;
                            }
                            else
                            {
                                userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) && (userInventory.UserState.ToLower() != "disabled") ? userClasification.SetStateTo : userInventory.UserState;
                            }
                            userInventory.BusinessUnit = !String.IsNullOrEmpty(userClasification.SetBusinessUnitTo) ? userClasification.SetBusinessUnitTo : userInventory.BusinessUnit;
                            userInventory.City = !String.IsNullOrEmpty(userClasification.SetCityTo) ? userClasification.SetCityTo : userInventory.City;
                            userInventory.Country = !String.IsNullOrEmpty(userClasification.SetCountryTo) ? userClasification.SetCountryTo : userInventory.Country;
                            userInventory.Continent = !String.IsNullOrEmpty(userClasification.SetContinentTo) ? userClasification.SetContinentTo : userInventory.Continent;
                            userInventory.Region = !String.IsNullOrEmpty(userClasification.SetRegionTo) ? userClasification.SetRegionTo : userInventory.Region;

                        }
                        else if (prop2.ToString() == userClasification.Object)
                        {
                            userInventory.UserRole = !String.IsNullOrEmpty(userClasification.SetRoleTo) ? userClasification.SetRoleTo : userInventory.UserRole;
                            userInventory.UserType = !String.IsNullOrEmpty(userClasification.SetTypeTo) ? userClasification.SetTypeTo : userInventory.UserType;
                            userInventory.UserClass = !String.IsNullOrEmpty(userClasification.SetClassTo) ? userClasification.SetClassTo : userInventory.UserClass;
                            if (userInventory.UserState == null)
                            {
                                userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) ? userClasification.SetStateTo : userInventory.UserState;
                            }
                            else
                            {
                                userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) && (userInventory.UserState.ToLower() != "disabled") ? userClasification.SetStateTo : userInventory.UserState;
                            }
                            userInventory.BusinessUnit = !String.IsNullOrEmpty(userClasification.SetBusinessUnitTo) ? userClasification.SetBusinessUnitTo : userInventory.BusinessUnit;
                            userInventory.City = !String.IsNullOrEmpty(userClasification.SetCityTo) ? userClasification.SetCityTo : userInventory.City;
                            userInventory.Country = !String.IsNullOrEmpty(userClasification.SetCountryTo) ? userClasification.SetCountryTo : userInventory.Country;
                            userInventory.Continent = !String.IsNullOrEmpty(userClasification.SetContinentTo) ? userClasification.SetContinentTo : userInventory.Continent;
                            userInventory.Region = !String.IsNullOrEmpty(userClasification.SetRegionTo) ? userClasification.SetRegionTo : userInventory.Region;

                        }
                    }
                    if (userClasification.Operator.Replace(" ", "").ToLower() == "is")
                    {
                        if (prop2 != null)
                        {
                            if (prop2.ToString().ToLower() == "true")
                            {
                                classificationRules += userClasification.ClassificationId + " (USR/" + userClasification.Type + "), ";
                                userInventory.UserRole = !String.IsNullOrEmpty(userClasification.SetRoleTo) ? userClasification.SetRoleTo : userInventory.UserRole;
                                userInventory.UserType = !String.IsNullOrEmpty(userClasification.SetTypeTo) ? userClasification.SetTypeTo : userInventory.UserType;
                                userInventory.UserClass = !String.IsNullOrEmpty(userClasification.SetClassTo) ? userClasification.SetClassTo : userInventory.UserClass;
                                if (userInventory.UserState == null)
                                {
                                    userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) ? userClasification.SetStateTo : userInventory.UserState;
                                }
                                else
                                {
                                    userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) && (userInventory.UserState.ToLower() != "disabled") ? userClasification.SetStateTo : userInventory.UserState;
                                }
                                userInventory.BusinessUnit = !String.IsNullOrEmpty(userClasification.SetBusinessUnitTo) ? userClasification.SetBusinessUnitTo : userInventory.BusinessUnit;
                                userInventory.City = !String.IsNullOrEmpty(userClasification.SetCityTo) ? userClasification.SetCityTo : userInventory.City;
                                userInventory.Country = !String.IsNullOrEmpty(userClasification.SetCountryTo) ? userClasification.SetCountryTo : userInventory.Country;
                                userInventory.Continent = !String.IsNullOrEmpty(userClasification.SetContinentTo) ? userClasification.SetContinentTo : userInventory.Continent;
                                userInventory.Region = !String.IsNullOrEmpty(userClasification.SetRegionTo) ? userClasification.SetRegionTo : userInventory.Region;

                            }
                            else if (prop2.ToString().ToLower() == userClasification.Object.ToLower())
                            {
                                classificationRules += userClasification.ClassificationId + " (USR/" + userClasification.Type + "), ";
                                userInventory.UserRole = !String.IsNullOrEmpty(userClasification.SetRoleTo) ? userClasification.SetRoleTo : userInventory.UserRole;
                                userInventory.UserType = !String.IsNullOrEmpty(userClasification.SetTypeTo) ? userClasification.SetTypeTo : userInventory.UserType;
                                userInventory.UserClass = !String.IsNullOrEmpty(userClasification.SetClassTo) ? userClasification.SetClassTo : userInventory.UserClass;
                                if (userInventory.UserState == null)
                                {
                                    userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) ? userClasification.SetStateTo : userInventory.UserState;
                                }
                                else
                                {
                                    userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) && (userInventory.UserState.ToLower() != "disabled") ? userClasification.SetStateTo : userInventory.UserState;
                                }
                                userInventory.BusinessUnit = !String.IsNullOrEmpty(userClasification.SetBusinessUnitTo) ? userClasification.SetBusinessUnitTo : userInventory.BusinessUnit;
                                userInventory.City = !String.IsNullOrEmpty(userClasification.SetCityTo) ? userClasification.SetCityTo : userInventory.City;
                                userInventory.Country = !String.IsNullOrEmpty(userClasification.SetCountryTo) ? userClasification.SetCountryTo : userInventory.Country;
                                userInventory.Continent = !String.IsNullOrEmpty(userClasification.SetContinentTo) ? userClasification.SetContinentTo : userInventory.Continent;
                                userInventory.Region = !String.IsNullOrEmpty(userClasification.SetRegionTo) ? userClasification.SetRegionTo : userInventory.Region;

                            }
                        }
                        else if (prop2 == null && String.IsNullOrEmpty(userClasification.Object))
                        {
                            classificationRules += userClasification.ClassificationId + " (USR/" + userClasification.Type + "), ";
                            userInventory.UserRole = !String.IsNullOrEmpty(userClasification.SetRoleTo) ? userClasification.SetRoleTo : userInventory.UserRole;
                            userInventory.UserType = !String.IsNullOrEmpty(userClasification.SetTypeTo) ? userClasification.SetTypeTo : userInventory.UserType;
                            userInventory.UserClass = !String.IsNullOrEmpty(userClasification.SetClassTo) ? userClasification.SetClassTo : userInventory.UserClass;
                            if (userInventory.UserState == null)
                            {
                                userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) ? userClasification.SetStateTo : userInventory.UserState;
                            }
                            else
                            {
                                userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) && (userInventory.UserState.ToLower() != "disabled") ? userClasification.SetStateTo : userInventory.UserState;
                            }
                            userInventory.BusinessUnit = !String.IsNullOrEmpty(userClasification.SetBusinessUnitTo) ? userClasification.SetBusinessUnitTo : userInventory.BusinessUnit;
                            userInventory.City = !String.IsNullOrEmpty(userClasification.SetCityTo) ? userClasification.SetCityTo : userInventory.City;
                            userInventory.Country = !String.IsNullOrEmpty(userClasification.SetCountryTo) ? userClasification.SetCountryTo : userInventory.Country;
                            userInventory.Continent = !String.IsNullOrEmpty(userClasification.SetContinentTo) ? userClasification.SetContinentTo : userInventory.Continent;
                            userInventory.Region = !String.IsNullOrEmpty(userClasification.SetRegionTo) ? userClasification.SetRegionTo : userInventory.Region;

                        }
                    }
                    if (userClasification.Operator.Replace(" ", "").ToLower() == "match")
                    {
                        //classificationRules += userClasification.ClassificationID + " (USC/" + userClasification.Type + "), ";
                    }
                    if (userClasification.Operator.Replace(" ", "").ToLower() == "like")
                    {
                        if (prop2 != null)
                        {
                            if (prop2.ToString().Contains(userClasification.Object.Replace("*", "")))
                            {
                                classificationRules += userClasification.ClassificationId + " (USR/" + userClasification.Type + "), ";
                                userInventory.UserRole = !String.IsNullOrEmpty(userClasification.SetRoleTo) ? userClasification.SetRoleTo : userInventory.UserRole;
                                userInventory.UserType = !String.IsNullOrEmpty(userClasification.SetTypeTo) ? userClasification.SetTypeTo : userInventory.UserType;
                                userInventory.UserClass = !String.IsNullOrEmpty(userClasification.SetClassTo) ? userClasification.SetClassTo : userInventory.UserClass;
                                if (userInventory.UserState == null)
                                {
                                    userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) ? userClasification.SetStateTo : userInventory.UserState;
                                }
                                else
                                {
                                    userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) && (userInventory.UserState.ToLower() != "disabled") ? userClasification.SetStateTo : userInventory.UserState;
                                }
                                userInventory.BusinessUnit = !String.IsNullOrEmpty(userClasification.SetBusinessUnitTo) ? userClasification.SetBusinessUnitTo : userInventory.BusinessUnit;
                                userInventory.City = !String.IsNullOrEmpty(userClasification.SetCityTo) ? userClasification.SetCityTo : userInventory.City;
                                userInventory.Country = !String.IsNullOrEmpty(userClasification.SetCountryTo) ? userClasification.SetCountryTo : userInventory.Country;
                                userInventory.Continent = !String.IsNullOrEmpty(userClasification.SetContinentTo) ? userClasification.SetContinentTo : userInventory.Continent;
                                userInventory.Region = !String.IsNullOrEmpty(userClasification.SetRegionTo) ? userClasification.SetRegionTo : userInventory.Region;

                            }
                        }
                    }
                    if (userClasification.Operator.Replace(" ", "").ToLower() == "notgreaterthan")
                    {
                        if (prop2 != null)
                        {
                            if (Convert.ToInt32(prop2) <= Convert.ToInt32(userClasification.Object))
                            {
                                classificationRules += userClasification.ClassificationId + " (USR/" + userClasification.Type + "), ";
                                userInventory.UserRole = !String.IsNullOrEmpty(userClasification.SetRoleTo) ? userClasification.SetRoleTo : userInventory.UserRole;
                                userInventory.UserType = !String.IsNullOrEmpty(userClasification.SetTypeTo) ? userClasification.SetTypeTo : userInventory.UserType;
                                userInventory.UserClass = !String.IsNullOrEmpty(userClasification.SetClassTo) ? userClasification.SetClassTo : userInventory.UserClass;
                                if (userInventory.UserState == null)
                                {
                                    userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) ? userClasification.SetStateTo : userInventory.UserState;
                                }
                                else
                                {
                                    userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) && (userInventory.UserState.ToLower() != "disabled") ? userClasification.SetStateTo : userInventory.UserState;
                                }
                                userInventory.BusinessUnit = !String.IsNullOrEmpty(userClasification.SetBusinessUnitTo) ? userClasification.SetBusinessUnitTo : userInventory.BusinessUnit;
                                userInventory.City = !String.IsNullOrEmpty(userClasification.SetCityTo) ? userClasification.SetCityTo : userInventory.City;
                                userInventory.Country = !String.IsNullOrEmpty(userClasification.SetCountryTo) ? userClasification.SetCountryTo : userInventory.Country;
                                userInventory.Continent = !String.IsNullOrEmpty(userClasification.SetContinentTo) ? userClasification.SetContinentTo : userInventory.Continent;
                                userInventory.Region = !String.IsNullOrEmpty(userClasification.SetRegionTo) ? userClasification.SetRegionTo : userInventory.Region;

                            }
                        }
                    }
                    if (userClasification.Operator.Replace(" ", "").ToLower() == "greaterthan")
                    {
                        if (prop2 != null)
                        {
                            if (Convert.ToInt32(prop2) > Convert.ToInt32(userClasification.Object))
                            {
                                classificationRules += userClasification.ClassificationId + " (USR/" + userClasification.Type + "), ";
                                userInventory.UserRole = !String.IsNullOrEmpty(userClasification.SetRoleTo) ? userClasification.SetRoleTo : userInventory.UserRole;
                                userInventory.UserType = !String.IsNullOrEmpty(userClasification.SetTypeTo) ? userClasification.SetTypeTo : userInventory.UserType;
                                userInventory.UserClass = !String.IsNullOrEmpty(userClasification.SetClassTo) ? userClasification.SetClassTo : userInventory.UserClass;
                                if (userInventory.UserState == null)
                                {
                                    userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) ? userClasification.SetStateTo : userInventory.UserState;
                                }
                                else
                                {
                                    userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) && (userInventory.UserState.ToLower() != "disabled") ? userClasification.SetStateTo : userInventory.UserState;
                                }
                                userInventory.BusinessUnit = !String.IsNullOrEmpty(userClasification.SetBusinessUnitTo) ? userClasification.SetBusinessUnitTo : userInventory.BusinessUnit;
                                userInventory.City = !String.IsNullOrEmpty(userClasification.SetCityTo) ? userClasification.SetCityTo : userInventory.City;
                                userInventory.Country = !String.IsNullOrEmpty(userClasification.SetCountryTo) ? userClasification.SetCountryTo : userInventory.Country;
                                userInventory.Continent = !String.IsNullOrEmpty(userClasification.SetContinentTo) ? userClasification.SetContinentTo : userInventory.Continent;
                                userInventory.Region = !String.IsNullOrEmpty(userClasification.SetRegionTo) ? userClasification.SetRegionTo : userInventory.Region;

                            }
                        }
                    }
                    if (userClasification.Operator.Replace(" ", "").ToLower() == "isempty")
                    {
                        if (prop2 == null)
                        {
                            classificationRules += userClasification.ClassificationId + " (USR/" + userClasification.Type + "), ";
                            userInventory.UserRole = !String.IsNullOrEmpty(userClasification.SetRoleTo) ? userClasification.SetRoleTo : userInventory.UserRole;
                            userInventory.UserType = !String.IsNullOrEmpty(userClasification.SetTypeTo) ? userClasification.SetTypeTo : userInventory.UserType;
                            userInventory.UserClass = !String.IsNullOrEmpty(userClasification.SetClassTo) ? userClasification.SetClassTo : userInventory.UserClass;
                            if (userInventory.UserState == null)
                            {
                                userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) ? userClasification.SetStateTo : userInventory.UserState;
                            }
                            else
                            {
                                userInventory.UserState = !String.IsNullOrEmpty(userClasification.SetStateTo) && (userInventory.UserState.ToLower() != "disabled") ? userClasification.SetStateTo : userInventory.UserState;
                            }
                            userInventory.BusinessUnit = !String.IsNullOrEmpty(userClasification.SetBusinessUnitTo) ? userClasification.SetBusinessUnitTo : userInventory.BusinessUnit;
                            userInventory.City = !String.IsNullOrEmpty(userClasification.SetCityTo) ? userClasification.SetCityTo : userInventory.City;
                            userInventory.Country = !String.IsNullOrEmpty(userClasification.SetCountryTo) ? userClasification.SetCountryTo : userInventory.Country;
                            userInventory.Continent = !String.IsNullOrEmpty(userClasification.SetContinentTo) ? userClasification.SetContinentTo : userInventory.Continent;
                            userInventory.Region = !String.IsNullOrEmpty(userClasification.SetRegionTo) ? userClasification.SetRegionTo : userInventory.Region;

                        }
                    }
                }
                userInventory.ClassificationRuleIds = classificationRules.TrimEnd(' ').TrimEnd(',');
            }

            return userInventories;
        }

        private List<UserInventory> ExecuteUserExceptions(List<UserInventory> userInventories, List<QualificationRule> listQualification)
        {
            #region execute UserExceptions rules

            #region create grouped exception list comboListUserExceptions
            //todo:clean/remove this
            //var comboListUserExceptions = new List<List<QualificationRule>>();

            //var newMultiList = new List<QualificationRule>();

            //var previousId = "";
            //foreach (var userException in listUserExceptions)
            //{
            //    if (String.IsNullOrEmpty(previousId))
            //    {
            //        previousId = userException.QualificationId;
            //    }

            //    if (previousId == userException.QualificationId)
            //    {
            //        newMultiList.Add(userException);
            //    }
            //    else
            //    {
            //        previousId = userException.QualificationId;
            //        comboListUserExceptions.Add(newMultiList);
            //        newMultiList = new List<QualificationRule>();
            //        newMultiList.Add(userException);
            //    }
            //}
            //if (newMultiList.Count != 0)
            //{
            //    comboListUserExceptions.Add(newMultiList);
            //    newMultiList = new List<QualificationRule>();
            //}
            #endregion
            var comboListQualifications = _adHelper.GetComboQualifications(listQualification);

            foreach (var userInventory in userInventories)
            {
                var exceptionRules = "";
                foreach (var listQualifications in comboListQualifications)
                {
                    var continueFlag = true;
                    foreach (var userException in listQualifications)
                    {
                        var prop = userException.Type.Replace(" ", "");
                        var prop2 = userInventory.GetType().GetProperty(prop).GetValue(userInventory);
                        if (userException.Operator.ToLower() == "is")
                        {
                            if (prop2 != null)
                            {
                                if (prop2.ToString() == userException.Object && continueFlag)
                                {

                                }
                                else
                                {
                                    continueFlag = false;
                                }
                                if (continueFlag && String.IsNullOrEmpty(userException.Condition))
                                {
                                    exceptionRules += userException.QualificationId + " (USR/" + userException.Type + "), ";

                                    if (!String.IsNullOrEmpty(userException.SetUserCountTo))
                                    {
                                        userInventory.UserCount = Convert.ToInt32(userException.SetUserCountTo);
                                    }
                                    if (!String.IsNullOrEmpty(userException.SetUserStateTo))
                                    {
                                        userInventory.UserState = userException.SetUserStateTo;
                                    }
                                    if (!String.IsNullOrEmpty(userException.SetUserStatusTo))
                                    {
                                        userInventory.UserStatus = userException.SetUserStatusTo;
                                    }
                                    //add still for user status
                                }
                            }
                        }
                        else if (userException.Operator.ToLower() == "not")
                        {
                            if (prop2 != null)
                            {
                                if (prop2.ToString() != userException.Object && continueFlag)
                                {

                                }
                                else
                                {
                                    continueFlag = false;
                                }
                                if (continueFlag && String.IsNullOrEmpty(userException.Condition))
                                {
                                    exceptionRules += userException.QualificationId + " (USR/" + userException.Type + "), ";
                                    if (!String.IsNullOrEmpty(userException.SetUserCountTo))
                                    {
                                        userInventory.UserCount = Convert.ToInt32(userException.SetUserCountTo);
                                    }
                                    if (!String.IsNullOrEmpty(userException.SetUserStateTo))
                                    {
                                        userInventory.UserState = userException.SetUserStateTo;
                                    }
                                    if (!String.IsNullOrEmpty(userException.SetUserStatusTo))
                                    {
                                        userInventory.UserStatus = userException.SetUserStatusTo;
                                    }
                                    //add still for user status
                                }
                            }
                        }
                    }
                }
                userInventory.QualificationRuleIds = exceptionRules.TrimEnd(' ').TrimEnd(',');
            }
            #endregion
            return userInventories;
        }
    }
}

﻿using LCXP.Origin.Domain;
using LCXP.Origin.Domain.ActiveDirectory;
using LCXP.Origin.Domain.Project.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LCXP.Origin.Contract.ActiveDirectory
{
    public class VerifyDevices
    {
        private ADHelpers _adHelper { get; set; }
        private readonly OriginContext _originContext;
        private List<DeviceInventory> _deviceInventories { get; set; }

        public VerifyDevices(OriginContext originContext, List<DeviceInventory> deviceInventories, string fileName)
        {
            _originContext = originContext;
            _deviceInventories = deviceInventories;
            _adHelper = new ADHelpers(fileName);
        }

        public List<DeviceInventory> VerifyAllDevices()
        {
            var listClassifications = _originContext.ClassificationRules.Where(x => x.RuleType == RuleType.Device).ToList();
            var listQualifications = _originContext.QualificationRules.Where(x => x.RuleType == RuleType.Device).ToList();

            _deviceInventories = ExecuteClassifications(_deviceInventories, listClassifications);
            _deviceInventories = ExecuteQualifications(_deviceInventories, listQualifications);

            return _deviceInventories;
        }

        private List<DeviceInventory> ExecuteClassifications(List<DeviceInventory> deviceInventories, List<ClassificationRule> listClassifications)
        {
            var listDeviceInventory = new List<DeviceInventory>();

            foreach (var deviceInventory in deviceInventories)
            {
                var classificationRules = "";
                foreach (var clasification in listClassifications)
                {
                    if (clasification.Operator.Replace(" ", "").ToLower() == "is")
                    {
                        if (clasification.DeviceType != null && clasification.DeviceType.Length > 1)
                        {
                            if (deviceInventory.DeviceType == clasification.DeviceType)
                            {
                                var classificationType = clasification.Type.Replace(" ", "");
                                var deviceInventoryTypeValue = deviceInventory.GetType().GetProperty(classificationType).GetValue(deviceInventory);
                                if (deviceInventoryTypeValue != null)
                                {
                                    if (clasification.Object.ToLower() == "y")
                                    {
                                        if (deviceInventoryTypeValue.ToString().ToLower() == "true") { }
                                    }
                                    else if (clasification.Object.ToLower() == "n")
                                    {
                                        if (deviceInventoryTypeValue.ToString().ToLower() == "false") { }
                                    }
                                    if (deviceInventoryTypeValue.ToString().ToLower() == clasification.Object)
                                    {
                                        classificationRules += clasification.ClassificationId + " (D/" + clasification.Type + "), ";//TODO: this might need to look at if the 1st rule

                                        //todo:check if there is any reason not to override for example something inactive/disabled
                                        deviceInventory.DeviceRole = !String.IsNullOrEmpty(clasification.SetRoleTo) ? clasification.SetRoleTo : deviceInventory.DeviceRole;
                                        deviceInventory.DeviceType = !String.IsNullOrEmpty(clasification.SetTypeTo) ? clasification.SetTypeTo : deviceInventory.DeviceType;
                                        deviceInventory.DeviceClass = !String.IsNullOrEmpty(clasification.SetClassTo) ? clasification.SetClassTo : deviceInventory.DeviceClass;
                                        deviceInventory.DeviceState = !String.IsNullOrEmpty(clasification.SetStateTo) ? clasification.SetStateTo : deviceInventory.DeviceState;
                                        deviceInventory.Environment = !String.IsNullOrEmpty(clasification.SetEnvironmentTo) ? clasification.SetEnvironmentTo : deviceInventory.Environment;

                                        deviceInventory.BusinessUnit = !String.IsNullOrEmpty(clasification.SetBusinessUnitTo) ? clasification.SetBusinessUnitTo : deviceInventory.BusinessUnit;
                                        deviceInventory.City = !String.IsNullOrEmpty(clasification.SetCityTo) ? clasification.SetCityTo : deviceInventory.City;
                                        deviceInventory.Country = !String.IsNullOrEmpty(clasification.SetCountryTo) ? clasification.SetCountryTo : deviceInventory.Country;
                                        deviceInventory.Continent = !String.IsNullOrEmpty(clasification.SetContinentTo) ? clasification.SetContinentTo : deviceInventory.Continent;
                                        deviceInventory.Region = !String.IsNullOrEmpty(clasification.SetRegionTo) ? clasification.SetRegionTo : deviceInventory.Region;

                                    }
                                }
                            }
                        }
                        else {

                        }
                    }
                    if (clasification.Operator.Replace(" ", "").ToLower() == "notgreaterthan")
                    {
                        if (clasification.DeviceType != null && clasification.DeviceType.Length > 1)
                        {
                            if (deviceInventory.DeviceType == clasification.DeviceType)
                            {
                                var classificationType = clasification.Type.Replace(" ", "");
                                var deviceInventoryTypeValue = deviceInventory.GetType().GetProperty(classificationType).GetValue(deviceInventory);

                                if (deviceInventoryTypeValue != null)
                                {
                                    if (Convert.ToInt32(deviceInventoryTypeValue) <= Convert.ToInt32(clasification.Object))
                                    {
                                        classificationRules += clasification.ClassificationId + " (D/" + clasification.Type + "), ";//TODO: this might need to look at if the 1st rule

                                        deviceInventory.DeviceRole = !String.IsNullOrEmpty(clasification.SetRoleTo) ? clasification.SetRoleTo : deviceInventory.DeviceRole;
                                        deviceInventory.DeviceType = !String.IsNullOrEmpty(clasification.SetTypeTo) ? clasification.SetTypeTo : deviceInventory.DeviceType;
                                        deviceInventory.DeviceClass = !String.IsNullOrEmpty(clasification.SetClassTo) ? clasification.SetClassTo : deviceInventory.DeviceClass;
                                        deviceInventory.DeviceState = !String.IsNullOrEmpty(clasification.SetStateTo) ? clasification.SetStateTo : deviceInventory.DeviceState;
                                        deviceInventory.Environment = !String.IsNullOrEmpty(clasification.SetEnvironmentTo) ? clasification.SetEnvironmentTo : deviceInventory.Environment;

                                        deviceInventory.BusinessUnit = !String.IsNullOrEmpty(clasification.SetBusinessUnitTo) ? clasification.SetBusinessUnitTo : deviceInventory.BusinessUnit;
                                        deviceInventory.City = !String.IsNullOrEmpty(clasification.SetCityTo) ? clasification.SetCityTo : deviceInventory.City;
                                        deviceInventory.Country = !String.IsNullOrEmpty(clasification.SetCountryTo) ? clasification.SetCountryTo : deviceInventory.Country;
                                        deviceInventory.Continent = !String.IsNullOrEmpty(clasification.SetContinentTo) ? clasification.SetContinentTo : deviceInventory.Continent;
                                        deviceInventory.Region = !String.IsNullOrEmpty(clasification.SetRegionTo) ? clasification.SetRegionTo : deviceInventory.Region;

                                    }
                                }
                            }
                        }
                        else
                        {

                        }
                    }
                    if (clasification.Operator.Replace(" ", "").ToLower() == "greaterthan")
                    {
                        if (clasification.DeviceType != null && clasification.DeviceType.Length > 1)
                        {

                            if (deviceInventory.DeviceType == clasification.DeviceType)
                            {
                                var classificationType = clasification.Type.Replace(" ", "");
                                var deviceInventoryTypeValue = deviceInventory.GetType().GetProperty(classificationType).GetValue(deviceInventory);
                                if (deviceInventoryTypeValue != null)
                                {
                                    if (Convert.ToInt32(deviceInventoryTypeValue) > Convert.ToInt32(clasification.Object))
                                    {
                                        classificationRules += clasification.ClassificationId + " (D/" + clasification.Type + "), ";//TODO: this might need to look at if the 1st rule

                                        deviceInventory.DeviceRole = !String.IsNullOrEmpty(clasification.SetRoleTo) ? clasification.SetRoleTo : deviceInventory.DeviceRole;
                                        deviceInventory.DeviceType = !String.IsNullOrEmpty(clasification.SetTypeTo) ? clasification.SetTypeTo : deviceInventory.DeviceType;
                                        deviceInventory.DeviceClass = !String.IsNullOrEmpty(clasification.SetClassTo) ? clasification.SetClassTo : deviceInventory.DeviceClass;
                                        deviceInventory.DeviceState = !String.IsNullOrEmpty(clasification.SetStateTo) ? clasification.SetStateTo : deviceInventory.DeviceState;
                                        deviceInventory.Environment = !String.IsNullOrEmpty(clasification.SetEnvironmentTo) ? clasification.SetEnvironmentTo : deviceInventory.Environment;

                                        deviceInventory.BusinessUnit = !String.IsNullOrEmpty(clasification.SetBusinessUnitTo) ? clasification.SetBusinessUnitTo : deviceInventory.BusinessUnit;
                                        deviceInventory.City = !String.IsNullOrEmpty(clasification.SetCityTo) ? clasification.SetCityTo : deviceInventory.City;
                                        deviceInventory.Country = !String.IsNullOrEmpty(clasification.SetCountryTo) ? clasification.SetCountryTo : deviceInventory.Country;
                                        deviceInventory.Continent = !String.IsNullOrEmpty(clasification.SetContinentTo) ? clasification.SetContinentTo : deviceInventory.Continent;
                                        deviceInventory.Region = !String.IsNullOrEmpty(clasification.SetRegionTo) ? clasification.SetRegionTo : deviceInventory.Region;

                                    }
                                }
                            }
                        }
                        else
                        {

                        }
                    }
                    if (clasification.Operator.Replace(" ", "").ToLower() == "isempty")
                    {
                        if (clasification.DeviceType != null && clasification.DeviceType.Length > 1)
                        {
                            if (deviceInventory.DeviceType == clasification.DeviceType)
                            {
                                var classificationType = clasification.Type.Replace(" ", "");
                                var deviceInventoryTypeValue = deviceInventory.GetType().GetProperty(classificationType).GetValue(deviceInventory);

                                if (deviceInventoryTypeValue == null)
                                {
                                    classificationRules += clasification.ClassificationId + " (D/" + clasification.Type + "), ";//TODO: this might need to look at if the 1st rule

                                    deviceInventory.DeviceRole = !String.IsNullOrEmpty(clasification.SetRoleTo) ? clasification.SetRoleTo : deviceInventory.DeviceRole;
                                    deviceInventory.DeviceType = !String.IsNullOrEmpty(clasification.SetTypeTo) ? clasification.SetTypeTo : deviceInventory.DeviceType;
                                    deviceInventory.DeviceClass = !String.IsNullOrEmpty(clasification.SetClassTo) ? clasification.SetClassTo : deviceInventory.DeviceClass;
                                    deviceInventory.DeviceState = !String.IsNullOrEmpty(clasification.SetStateTo) ? clasification.SetStateTo : deviceInventory.DeviceState;
                                    deviceInventory.Environment = !String.IsNullOrEmpty(clasification.SetEnvironmentTo) ? clasification.SetEnvironmentTo : deviceInventory.Environment;

                                    deviceInventory.BusinessUnit = !String.IsNullOrEmpty(clasification.SetBusinessUnitTo) ? clasification.SetBusinessUnitTo : deviceInventory.BusinessUnit;
                                    deviceInventory.City = !String.IsNullOrEmpty(clasification.SetCityTo) ? clasification.SetCityTo : deviceInventory.City;
                                    deviceInventory.Country = !String.IsNullOrEmpty(clasification.SetCountryTo) ? clasification.SetCountryTo : deviceInventory.Country;
                                    deviceInventory.Continent = !String.IsNullOrEmpty(clasification.SetContinentTo) ? clasification.SetContinentTo : deviceInventory.Continent;
                                    deviceInventory.Region = !String.IsNullOrEmpty(clasification.SetRegionTo) ? clasification.SetRegionTo : deviceInventory.Region;

                                }
                            }
                        }
                        else
                        {

                        }
                    }
                    if (clasification.Operator.Replace(" ", "").ToLower() == "match")
                    {
                        //todo: add logic for match
                        if (clasification.DeviceType != null && clasification.DeviceType.Length > 1)
                        {
                            if (deviceInventory.DeviceType == clasification.DeviceType)
                            {
                                var classificationType = clasification.Type.Replace(" ", "");
                                var deviceInventoryTypeValue = deviceInventory.GetType().GetProperty(classificationType).GetValue(deviceInventory);
                            }
                        }
                        else
                        {

                        }
                    }
                    if (clasification.Operator.Replace(" ", "").ToLower() == "like")
                    {
                        //todo: add logic for match
                        if (clasification.DeviceType != null && clasification.DeviceType.Length > 1)
                        {
                            if (deviceInventory.DeviceType == clasification.DeviceType)
                            {
                                
                            }
                        }
                        else
                        {
                            var classificationType = clasification.Type.Replace(" ", "");
                            var deviceInventoryTypeValue = deviceInventory.GetType().GetProperty(classificationType).GetValue(deviceInventory);

                            if (clasification.DeviceType == "*")
                            {
                                if (clasification.Object.StartsWith("*") && clasification.Object.EndsWith("*"))
                                {
                                    if (deviceInventoryTypeValue.ToString().Contains(clasification.Object.Trim('*')))
                                    {
                                        classificationRules += clasification.ClassificationId + " (D/" + clasification.Type + "), ";//TODO: this might need to look at if the 1st rule

                                        //todo:check if there is any reason not to override for example something inactive/disabled
                                        deviceInventory.DeviceRole = !String.IsNullOrEmpty(clasification.SetRoleTo) ? clasification.SetRoleTo : deviceInventory.DeviceRole;
                                        deviceInventory.DeviceType = !String.IsNullOrEmpty(clasification.SetTypeTo) ? clasification.SetTypeTo : deviceInventory.DeviceType;
                                        deviceInventory.DeviceClass = !String.IsNullOrEmpty(clasification.SetClassTo) ? clasification.SetClassTo : deviceInventory.DeviceClass;
                                        deviceInventory.DeviceState = !String.IsNullOrEmpty(clasification.SetStateTo) ? clasification.SetStateTo : deviceInventory.DeviceState;
                                        deviceInventory.Environment = !String.IsNullOrEmpty(clasification.SetEnvironmentTo) ? clasification.SetEnvironmentTo : deviceInventory.Environment;

                                        deviceInventory.BusinessUnit = !String.IsNullOrEmpty(clasification.SetBusinessUnitTo) ? clasification.SetBusinessUnitTo : deviceInventory.BusinessUnit;
                                        deviceInventory.City = !String.IsNullOrEmpty(clasification.SetCityTo) ? clasification.SetCityTo : deviceInventory.City;
                                        deviceInventory.Country = !String.IsNullOrEmpty(clasification.SetCountryTo) ? clasification.SetCountryTo : deviceInventory.Country;
                                        deviceInventory.Continent = !String.IsNullOrEmpty(clasification.SetContinentTo) ? clasification.SetContinentTo : deviceInventory.Continent;
                                        deviceInventory.Region = !String.IsNullOrEmpty(clasification.SetRegionTo) ? clasification.SetRegionTo : deviceInventory.Region;

                                    }
                                }
                                else if (clasification.Object.StartsWith("*") && !clasification.Object.EndsWith("*"))
                                {
                                    if (deviceInventoryTypeValue.ToString().EndsWith(clasification.Object.Trim('*')))
                                    {
                                        classificationRules += clasification.ClassificationId + " (D/" + clasification.Type + "), ";//TODO: this might need to look at if the 1st rule

                                        //todo:check if there is any reason not to override for example something inactive/disabled
                                        deviceInventory.DeviceRole = !String.IsNullOrEmpty(clasification.SetRoleTo) ? clasification.SetRoleTo : deviceInventory.DeviceRole;
                                        deviceInventory.DeviceType = !String.IsNullOrEmpty(clasification.SetTypeTo) ? clasification.SetTypeTo : deviceInventory.DeviceType;
                                        deviceInventory.DeviceClass = !String.IsNullOrEmpty(clasification.SetClassTo) ? clasification.SetClassTo : deviceInventory.DeviceClass;
                                        deviceInventory.DeviceState = !String.IsNullOrEmpty(clasification.SetStateTo) ? clasification.SetStateTo : deviceInventory.DeviceState;
                                        deviceInventory.Environment = !String.IsNullOrEmpty(clasification.SetEnvironmentTo) ? clasification.SetEnvironmentTo : deviceInventory.Environment;

                                        deviceInventory.BusinessUnit = !String.IsNullOrEmpty(clasification.SetBusinessUnitTo) ? clasification.SetBusinessUnitTo : deviceInventory.BusinessUnit;
                                        deviceInventory.City = !String.IsNullOrEmpty(clasification.SetCityTo) ? clasification.SetCityTo : deviceInventory.City;
                                        deviceInventory.Country = !String.IsNullOrEmpty(clasification.SetCountryTo) ? clasification.SetCountryTo : deviceInventory.Country;
                                        deviceInventory.Continent = !String.IsNullOrEmpty(clasification.SetContinentTo) ? clasification.SetContinentTo : deviceInventory.Continent;
                                        deviceInventory.Region = !String.IsNullOrEmpty(clasification.SetRegionTo) ? clasification.SetRegionTo : deviceInventory.Region;

                                    }
                                }
                                else if (!clasification.Object.StartsWith("*") && clasification.Object.EndsWith("*"))
                                {
                                    if (clasification.Object.StartsWith("?"))
                                    {
                                        var countQs = clasification.Object.Length - clasification.Object.Replace("?", "").Length;
                                        if (deviceInventoryTypeValue.ToString().Length > countQs)
                                        {
                                            var deviceTypeValueShorten = deviceInventoryTypeValue.ToString().Substring(countQs);
                                            if (deviceTypeValueShorten.StartsWith(clasification.Object.Trim('*').Trim('?')))
                                            {
                                                classificationRules += clasification.ClassificationId + " (D/" + clasification.Type + "), ";//TODO: this might need to look at if the 1st rule

                                                //todo:check if there is any reason not to override for example something inactive/disabled
                                                deviceInventory.DeviceRole = !String.IsNullOrEmpty(clasification.SetRoleTo) ? clasification.SetRoleTo : deviceInventory.DeviceRole;
                                                deviceInventory.DeviceType = !String.IsNullOrEmpty(clasification.SetTypeTo) ? clasification.SetTypeTo : deviceInventory.DeviceType;
                                                deviceInventory.DeviceClass = !String.IsNullOrEmpty(clasification.SetClassTo) ? clasification.SetClassTo : deviceInventory.DeviceClass;
                                                deviceInventory.DeviceState = !String.IsNullOrEmpty(clasification.SetStateTo) ? clasification.SetStateTo : deviceInventory.DeviceState;
                                                deviceInventory.Environment = !String.IsNullOrEmpty(clasification.SetEnvironmentTo) ? clasification.SetEnvironmentTo : deviceInventory.Environment;

                                                deviceInventory.BusinessUnit = !String.IsNullOrEmpty(clasification.SetBusinessUnitTo) ? clasification.SetBusinessUnitTo : deviceInventory.BusinessUnit;
                                                deviceInventory.City = !String.IsNullOrEmpty(clasification.SetCityTo) ? clasification.SetCityTo : deviceInventory.City;
                                                deviceInventory.Country = !String.IsNullOrEmpty(clasification.SetCountryTo) ? clasification.SetCountryTo : deviceInventory.Country;
                                                deviceInventory.Continent = !String.IsNullOrEmpty(clasification.SetContinentTo) ? clasification.SetContinentTo : deviceInventory.Continent;
                                                deviceInventory.Region = !String.IsNullOrEmpty(clasification.SetRegionTo) ? clasification.SetRegionTo : deviceInventory.Region;

                                            }
                                        }
                                    }
                                    else if (deviceInventoryTypeValue.ToString().StartsWith(clasification.Object.Trim('*')))
                                    {
                                        classificationRules += clasification.ClassificationId + " (D/" + clasification.Type + "), ";//TODO: this might need to look at if the 1st rule

                                        //todo:check if there is any reason not to override for example something inactive/disabled
                                        deviceInventory.DeviceRole = !String.IsNullOrEmpty(clasification.SetRoleTo) ? clasification.SetRoleTo : deviceInventory.DeviceRole;
                                        deviceInventory.DeviceType = !String.IsNullOrEmpty(clasification.SetTypeTo) ? clasification.SetTypeTo : deviceInventory.DeviceType;
                                        deviceInventory.DeviceClass = !String.IsNullOrEmpty(clasification.SetClassTo) ? clasification.SetClassTo : deviceInventory.DeviceClass;
                                        deviceInventory.DeviceState = !String.IsNullOrEmpty(clasification.SetStateTo) ? clasification.SetStateTo : deviceInventory.DeviceState;
                                        deviceInventory.Environment = !String.IsNullOrEmpty(clasification.SetEnvironmentTo) ? clasification.SetEnvironmentTo : deviceInventory.Environment;

                                        deviceInventory.BusinessUnit = !String.IsNullOrEmpty(clasification.SetBusinessUnitTo) ? clasification.SetBusinessUnitTo : deviceInventory.BusinessUnit;
                                        deviceInventory.City = !String.IsNullOrEmpty(clasification.SetCityTo) ? clasification.SetCityTo : deviceInventory.City;
                                        deviceInventory.Country = !String.IsNullOrEmpty(clasification.SetCountryTo) ? clasification.SetCountryTo : deviceInventory.Country;
                                        deviceInventory.Continent = !String.IsNullOrEmpty(clasification.SetContinentTo) ? clasification.SetContinentTo : deviceInventory.Continent;
                                        deviceInventory.Region = !String.IsNullOrEmpty(clasification.SetRegionTo) ? clasification.SetRegionTo : deviceInventory.Region;

                                    }
                                }
                            }
                        }
                    }

                }
                deviceInventory.ClassificationRuleIds = classificationRules.TrimEnd(' ').TrimEnd(',');
                listDeviceInventory.Add(deviceInventory);
            }

            return listDeviceInventory;
        }

        private List<DeviceInventory> ExecuteQualifications(List<DeviceInventory> deviceInventories, List<QualificationRule> listQualifications)
        {
            var listDeviceInventory = new List<DeviceInventory>();
            var comboQualifications = _adHelper.GetComboQualifications(listQualifications);

            foreach (var deviceInventory in deviceInventories)
            {
                var qualificationRules = "";
                foreach (var listQualification in comboQualifications)
                {
                    var continueFlag = true;
                    foreach (var qualification in listQualification)
                    {
                        var qualificationsType = qualification.Type.Replace(" ", "");
                        var deviceInventoryTypeValue = deviceInventory.GetType().GetProperty(qualificationsType).GetValue(deviceInventory);
                        if (deviceInventory.DeviceId == "D0062")
                        {
                            if (qualification.QualificationId == "DQ00E")
                            {

                            }
                        }
                        if (qualification.Operator.ToLower() == "is")
                        {
                            if (deviceInventoryTypeValue != null)
                            {
                                if (qualification.Object.ToLower() == "y")
                                { qualification.Object = "true"; }
                                if (qualification.Object.ToLower() == "n")
                                { qualification.Object = "false"; }
                                if (deviceInventoryTypeValue.ToString().ToLower() == qualification.Object.ToLower() && continueFlag)
                                {

                                }
                                else
                                {
                                    continueFlag = false;
                                }
                                if (continueFlag && String.IsNullOrEmpty(qualification.Condition))
                                {
                                    qualificationRules += qualification.QualificationId + " (D/" + qualification.Type + "), ";

                                    if (qualification.SetOfficeEligibleDeviceTo != null)
                                    {
                                        deviceInventory.OfficeEligibleDevice = qualification.SetOfficeEligibleDeviceTo;
                                    }
                                    if (!String.IsNullOrEmpty(qualification.SetDeviceStateTo))
                                    {
                                        deviceInventory.DeviceState = qualification.SetDeviceStateTo;
                                    }
                                    if (!String.IsNullOrEmpty(qualification.SetDeviceStatusTo))
                                    {
                                        deviceInventory.DeviceStatus = qualification.SetDeviceStatusTo;
                                    }
                                    if (qualification.SetOfficeEligibleDeviceTo != null)
                                    {
                                        deviceInventory.OfficeEligibleDevice = qualification.SetOfficeEligibleDeviceTo;
                                    }
                                    if (qualification.SetExclusionReasonTo != null)
                                    {
                                        deviceInventory.ExclusionReason = qualification.SetExclusionReasonTo;
                                    }
                                    if (qualification.SetObservationTypeTo != null)
                                    {
                                        deviceInventory.ObservationsType = qualification.SetObservationTypeTo;
                                    }
                                    if (qualification.SetObservationTo != null)
                                    {
                                        deviceInventory.Observations = qualification.SetObservationTo;
                                    }
                                    if (qualification.SetActionTo != null)
                                    {
                                        deviceInventory.Action = qualification.SetActionTo;
                                    }
                                }
                            }
                        }
                        else if (qualification.Operator.ToLower() == "not")
                        {
                            if (deviceInventoryTypeValue != null)
                            {
                                if ((deviceInventoryTypeValue.ToString().ToLower() != qualification.Object.ToLower()) && continueFlag)
                                {

                                }
                                else
                                {
                                    continueFlag = false;
                                }
                                if (continueFlag && String.IsNullOrEmpty(qualification.Condition))
                                {
                                    qualificationRules += qualification.QualificationId + " (D/" + qualification.Type + "), ";

                                    if (qualification.SetOfficeEligibleDeviceTo != null)
                                    {
                                        deviceInventory.OfficeEligibleDevice = qualification.SetOfficeEligibleDeviceTo;
                                    }
                                    if (!String.IsNullOrEmpty(qualification.SetDeviceStateTo))
                                    {
                                        deviceInventory.DeviceState = qualification.SetDeviceStateTo;
                                    }
                                    if (!String.IsNullOrEmpty(qualification.SetDeviceStatusTo))
                                    {
                                        deviceInventory.DeviceStatus = qualification.SetDeviceStatusTo;
                                    }
                                    if (qualification.SetOfficeEligibleDeviceTo != null)
                                    {
                                        deviceInventory.OfficeEligibleDevice = qualification.SetOfficeEligibleDeviceTo;
                                    }
                                    if (qualification.SetExclusionReasonTo != null)
                                    {
                                        deviceInventory.ExclusionReason = qualification.SetExclusionReasonTo;
                                    }
                                    if (qualification.SetObservationTypeTo != null)
                                    {
                                        deviceInventory.ObservationsType = qualification.SetObservationTypeTo;
                                    }
                                    if (qualification.SetObservationTo != null)
                                    {
                                        deviceInventory.Observations = qualification.SetObservationTo;
                                    }
                                    if (qualification.SetActionTo != null)
                                    {
                                        deviceInventory.Action = qualification.SetActionTo;
                                    }
                                }
                            }
                        }
                        else if (qualification.Operator.ToLower() == "like")
                        {
                            //todo: check starts with(ch*) or end(*ch) or full(*ch*) or from char (???ch*)
                            if (deviceInventoryTypeValue != null)
                            {
                                if (deviceInventoryTypeValue.ToString().ToLower().Contains(qualification.Object.Replace("*","").ToLower()) && continueFlag)
                                {

                                }
                                else
                                {
                                    continueFlag = false;
                                }
                                if (continueFlag && String.IsNullOrEmpty(qualification.Condition))
                                {
                                    qualificationRules += qualification.QualificationId + " (D/" + qualification.Type + "), ";

                                    if (qualification.SetOfficeEligibleDeviceTo != null)
                                    {
                                        deviceInventory.OfficeEligibleDevice = qualification.SetOfficeEligibleDeviceTo;
                                    }
                                    if (!String.IsNullOrEmpty(qualification.SetDeviceStateTo))
                                    {
                                        deviceInventory.DeviceState = qualification.SetDeviceStateTo;
                                    }
                                    if (!String.IsNullOrEmpty(qualification.SetDeviceStatusTo))
                                    {
                                        deviceInventory.DeviceStatus = qualification.SetDeviceStatusTo;
                                    }
                                    if (qualification.SetOfficeEligibleDeviceTo != null)
                                    {
                                        deviceInventory.OfficeEligibleDevice = qualification.SetOfficeEligibleDeviceTo;
                                    }
                                    if (qualification.SetExclusionReasonTo != null)
                                    {
                                        deviceInventory.ExclusionReason = qualification.SetExclusionReasonTo;
                                    }
                                    if (qualification.SetObservationTypeTo != null)
                                    {
                                        deviceInventory.ObservationsType = qualification.SetObservationTypeTo;
                                    }
                                    if (qualification.SetObservationTo != null)
                                    {
                                        deviceInventory.Observations = qualification.SetObservationTo;
                                    }
                                    if (qualification.SetActionTo != null)
                                    {
                                        deviceInventory.Action = qualification.SetActionTo;
                                    }
                                }
                            }
                        }
                        else if (qualification.Operator.ToLower() == "isempty")
                        {
                            if (deviceInventoryTypeValue == null)
                            {
                            }
                            else
                            {
                                continueFlag = false;
                            }
                            if (continueFlag && String.IsNullOrEmpty(qualification.Condition))
                            {
                                qualificationRules += qualification.QualificationId + " (D/" + qualification.Type + "), ";

                                if (qualification.SetOfficeEligibleDeviceTo != null)
                                {
                                    deviceInventory.OfficeEligibleDevice = qualification.SetOfficeEligibleDeviceTo;
                                }
                                if (!String.IsNullOrEmpty(qualification.SetDeviceStateTo))
                                {
                                    deviceInventory.DeviceState = qualification.SetDeviceStateTo;
                                }
                                if (!String.IsNullOrEmpty(qualification.SetDeviceStatusTo))
                                {
                                    deviceInventory.DeviceStatus = qualification.SetDeviceStatusTo;
                                }
                                if (qualification.SetOfficeEligibleDeviceTo != null)
                                {
                                    deviceInventory.OfficeEligibleDevice = qualification.SetOfficeEligibleDeviceTo;
                                }
                                if (qualification.SetExclusionReasonTo != null)
                                {
                                    deviceInventory.ExclusionReason = qualification.SetExclusionReasonTo;
                                }
                                if (qualification.SetObservationTypeTo != null)
                                {
                                    deviceInventory.ObservationsType = qualification.SetObservationTypeTo;
                                }
                                if (qualification.SetObservationTo != null)
                                {
                                    deviceInventory.Observations = qualification.SetObservationTo;
                                }
                                if (qualification.SetActionTo != null)
                                {
                                    deviceInventory.Action = qualification.SetActionTo;
                                }
                            }
                        }
                    }
                }
                deviceInventory.QualificationRuleIds = qualificationRules.TrimEnd(' ').TrimEnd(',');
                listDeviceInventory.Add(deviceInventory);
            }
           
            return listDeviceInventory;
        }
    }
}

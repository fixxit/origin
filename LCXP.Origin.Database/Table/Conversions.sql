﻿CREATE TABLE [dbo].[OSConversions]
(
    [Id] [bigint] IDENTITY(1,1) NOT NULL,
    [Source] [nvarchar](50) NULL,
    [OSId] [nvarchar](255) NULL,
    [Manufacturer] [nvarchar](100) NULL,
    [ProductFamily] [nvarchar](100) NULL,
    [Product] [nvarchar](50) NULL,
    [ProductVersion] [nvarchar](50) NULL,
    [IsLicensable] [nvarchar](50) NULL,
    [Quantity] [nvarchar](15) NULL,
    [Type] [nvarchar](50) NULL,
    [LicenseMetric] [nvarchar](50) NULL
)
﻿CREATE TABLE [dbo].[OrganizationalUnit]
(
    [OrganizationalUnitId] [bigint] IDENTITY(1,1) NOT NULL,
    [OuId] [nvarchar](10) NULL,
    [Id] [nvarchar](100) NULL,
    [DistinguishedName] [nvarchar](255) NULL,
    [_OrganizationalUnit] [nvarchar](255) NULL,
    [Domain] [nvarchar](255) NULL,
    [Comments] [nvarchar](max) NULL
)
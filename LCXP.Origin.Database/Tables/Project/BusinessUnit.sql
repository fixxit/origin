﻿CREATE TABLE [project].[BusinessUnit]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY DEFAULT NEWSEQUENTIALID(),
	[Name] VARCHAR(50) NOT NULL,
	[City] VARCHAR(50) NULL,
	[Country] VARCHAR(50) NULL,
	[Continent] VARCHAR(50) NULL,
	[Region] VARCHAR(50) NULL,
	[ConfigurationId] UNIQUEIDENTIFIER NOT NULL,
	FOREIGN KEY ([ConfigurationId]) REFERENCES [project].[Configuration] ([Id])
);

﻿CREATE TABLE [project].[ClassificationRule]
(
	[Id] BIGINT IDENTITY(1,1) NOT NULL,
	[ClassificationId] VARCHAR(50) NULL,
	[Type] VARCHAR(50) NULL,
	[Operator] VARCHAR(50) NULL,
	[Object] VARCHAR(50) NULL,
	[Source] VARCHAR(50) NULL,
	[SetRoleTo] VARCHAR(50) NULL,
	[SetTypeTo] VARCHAR(50) NULL,
	[SetClassTo] VARCHAR(50) NULL,
	[SetStateTo] VARCHAR(50) NULL,
	[SetBusinessUnitTo] [nvarchar](max) NULL,
    [SetCityTo] [nvarchar](max) NULL,
    [SetCountryTo] [nvarchar](max) NULL,
    [SetContinentTo] [nvarchar](max) NULL,
    [SetRegionTo] [nvarchar](max) NULL,
    [ClassificationTarget] [nvarchar](max) NULL,
    [DeviceType] [nvarchar](50) NULL,
    [SetEnvironmentTo] [nvarchar](50) NULL,
    [RuleType] INT NOT NULL,
	[ProjectId] UNIQUEIDENTIFIER NULL,
	[BusinessUnitId] UNIQUEIDENTIFIER NULL,
	FOREIGN KEY ([ProjectId]) REFERENCES [project].[Project] ([Id])
);

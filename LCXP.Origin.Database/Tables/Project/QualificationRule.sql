﻿CREATE TABLE [project].[QualificationRule]
(
	[Id] BIGINT IDENTITY(1,1) NOT NULL,
	[QualificationId] VARCHAR(50) NULL,
    [Type] VARCHAR(50) NULL,
    [Operator] VARCHAR(50) NULL,
	[Object] VARCHAR(50) NULL,
	[Condition] VARCHAR(50) NULL,
	[Source] VARCHAR(50) NULL,
	[SetUserCountTo] VARCHAR(50) NULL,
	[SetUserStateTo] VARCHAR(50) NULL,
	[SetUserStatusTo] VARCHAR(50) NULL,
	[SetExclusionReasonTo] VARCHAR(255) NULL,
	[RuleType] INT NOT NULL,
	[BusinessUnitId] UNIQUEIDENTIFIER NULL,
	[ProjectId] UNIQUEIDENTIFIER NULL,
	[SetDeviceStateTo] nvarchar(50),
	[SetDeviceStatusTo] nvarchar(50),
	[SetOfficeEligibleDeviceTo] bit,
	[SetObservationTypeTo] nvarchar(50),
	[SetObservationTo] nvarchar(255),
	[SetActionTo] nvarchar(255)
	FOREIGN KEY ([ProjectId]) REFERENCES [project].[Project] ([Id])
);

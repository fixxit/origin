﻿CREATE TABLE [project].[Configuration]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY DEFAULT NEWSEQUENTIALID(),
	[Parent] VARCHAR(200) NULL,
	[ManagedManufacturers] VARCHAR(200) NULL,
	[DateFormat] VARCHAR(50) NULL,
	[ReferenceDate] DATE NULL,
	[SnapshotDate] DATE NULL,
    [FilePath] VARCHAR(255) NOT NULL,
	[ProjectId] UNIQUEIDENTIFIER NOT NULL,
	FOREIGN KEY ([ProjectId]) REFERENCES [project].[Project] ([Id])
);

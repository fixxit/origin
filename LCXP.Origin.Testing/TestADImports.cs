﻿using System;
using System.Linq;
using LCXP.Origin.Contract.ActiveDirectory;
using LCXP.Origin.Domain;
using LCXP.Origin.Domain.ActiveDirectory;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LCXP.Origin.Testing
{
    [TestClass]
    public class TestADImports
    {
        private OriginContext _originContext = new OriginContext("AzureContext");
        public string _file = @"C:\Users\chris\source\UnitTestProject1\Files\adexport_20190503.csv";

        [TestMethod]
        public void TestCleanUsersAndDevices()
        {
            _originContext.Database.ExecuteSqlCommand("TRUNCATE TABLE UserInventory");
            //_originContext.UserInventory..SaveChanges();
            _originContext.Database.ExecuteSqlCommand("TRUNCATE TABLE [dbo].[DeviceInventory]");

            //_originContext.UserInventory.RemoveRange(_originContext.UserInventory);
            //_originContext.SaveChanges();
        }

        [TestMethod]
        public void TestFullFlow()
        {
            #region Import Users
            _originContext = new OriginContext("OriginContext");
            var importUsers = new ImportUsers(_originContext, _file);
            importUsers.ImportUserInventory();
           
            #endregion
            #region Import Devices
            _originContext = new OriginContext("OriginContext");
            var importDevices = new ImportDevices(_originContext, _file);
            importDevices.ImportDeviceInventory();
            
            #endregion
            #region Import OU
            _originContext = new OriginContext("OriginContext");
            var importGroupInfo = new ImportGroupInfo(_file);
            var listGroups = importGroupInfo.GetGroupInfo();

            foreach (var group in listGroups)
            {
                _originContext.ActiveDirectoryGroup.Add(group);
            }
            _originContext.SaveChanges();
            #endregion
            #region Import AD Group
            _originContext = new OriginContext("OriginContext");
            var importOU = new ImportOU(_file);
            var listOU = importOU.GetOUs();

            foreach (var ou in listOU)
            {
                _originContext.OrganizationalUnit.Add(ou);
                //db.SaveChanges();//per record
            }
            _originContext.SaveChanges();//bulk
            #endregion
        }

        [TestMethod]
        public void TestImportDeviceInventory()
        {
            _originContext = new OriginContext("AzureContext");
            _file = @"C:\LCXP\jfhillebrand\adexport_20190503.csv";
            //_file = @"C:\LCXP\jfhillebrand\lagena_adexport_20190503.csv";

            var importDevices = new ImportDevices(_originContext,_file);
            importDevices.ImportDeviceInventory();
        }
        
        [TestMethod]
        public void TestLoadAdGroup()
        {
            _originContext = new OriginContext("AzureContext");
            var importGroupInfo = new ImportGroupInfo(_file);
            var listGroups = importGroupInfo.GetGroupInfo();

            foreach (var group in listGroups)
            {
                _originContext.ActiveDirectoryGroup.Add(group);
            }
            _originContext.SaveChanges();
        }

        [TestMethod]
        public void TestOULoad()
        {
            _originContext = new OriginContext("AzureContext");
            
            var importOU = new ImportOU(_file);
            var listOU = importOU.GetOUs();
            
            foreach (var ou in listOU)
            {
            _originContext.OrganizationalUnit.Add(ou);
                //db.SaveChanges();//per record
            }
            _originContext.SaveChanges();//bulk

        }

        [TestMethod]
        public void TestImportUserInventory()
        {
            _originContext = new OriginContext("AzureContext");
            _file = @"C:\LCXP\jfhillebrand\adexport_20190503.csv";

            var importUsers = new ImportUsers(_originContext, _file);
            importUsers.ImportUserInventory();

        }
    }
}

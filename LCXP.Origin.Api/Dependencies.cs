﻿using LCXP.Origin.Domain;
using LCXP.Origin.Domain.Client;
using LCXP.Origin.Domain.Import;
using LCXP.Origin.Domain.Project;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LCXP.Origin.Api
{
    public class Dependencies
    {
        internal static void Register(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<OriginContext>(s => new OriginContext(configuration["ConnectionStrings:DefaultConnection"]));
            services.AddScoped<IClientRepository, ClientRepository>();
            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<IImportRepository, ImportRepository>();
        }
    }
}

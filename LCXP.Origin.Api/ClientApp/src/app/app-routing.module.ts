import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../app/pages/home/home.component';
import { ClientComponent } from '../app/pages/client/client.component';
import { ProjectComponent } from './pages/project/project.component';
import { ActiveDirectoryModuleComponent } from './pages/module/active-directory-module/active-directory-module.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'client/:clientId', component: ClientComponent },
  { path: 'client/:clientId/project/:projectId', component: ProjectComponent },
  { path: 'client/:clientId/project/:projectId/active-directory', component: ActiveDirectoryModuleComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: []
})

export class AppRoutingModule { }

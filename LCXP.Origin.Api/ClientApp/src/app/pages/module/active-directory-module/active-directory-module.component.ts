import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from 'src/services/project.service';
import { Configuration, RuleType, ClassificationRule, QualificationRule, UserInventory, DeviceInventory, BusinessUnit } from 'models';
import { ImportService } from 'src/services/import.service';

@Component({
  selector: 'app-active-directory-module',
  templateUrl: './active-directory-module.component.html',
  providers: [ProjectService, ImportService]
})
export class ActiveDirectoryModuleComponent implements OnInit {

  userRuleType: RuleType = RuleType.user;
  deviceRuleType: RuleType = RuleType.device;
  projectId: string;
  existingConfiguration: Configuration = null;
  businessUnits: BusinessUnit[] = [];
  userClassificationRules: ClassificationRule[] = [];
  deviceClassificationRules: ClassificationRule[] = [];
  userQualificationRules: QualificationRule[] = [];
  deviceQualificationRules: QualificationRule[] = [];
  userInventoryImportResult: UserInventory[] = [];
  deviceInventoryImportResult: DeviceInventory[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private projectService: ProjectService,
    private importService: ImportService
  ) { }

  ngOnInit() {
  }

  fetchProjectConfiguration = () => {
    const self = this;
    self.projectId = self.activatedRoute.snapshot.params['projectId'];
    self.projectService.getProjectConfiguration(self.projectId)
      .subscribe(configuration => {
        self.existingConfiguration = configuration;
      }, error => {
        // TODO: Add error handling
        console.error(error);
      });
  }

  fetchBusinessUnits = () => {
    const self = this;
    self.projectId = self.activatedRoute.snapshot.params['projectId'];
    self.projectService.getBusinessUnits(self.projectId)
      .subscribe(businessUnits => {
        self.businessUnits = businessUnits;
      }, error => {
        // TODO Add error handling
        console.error(error);
      });
  }

  deleteBusinessUnit = (id: string) => {
    const self = this;
    self.projectService.deleteBusinessUnit(id)
      .subscribe(() => {
        self.fetchBusinessUnits();
      });
  }

  saveProjectConfiguration = (config: Configuration) => {
    const self = this;
    self.projectService.saveProjectConfiguration(config)
      .subscribe(() => {
        self.fetchProjectConfiguration();
        self.fetchBusinessUnits();
      }, error => {
        // TODO: Add error handling
        console.error(error);
      });
  }

  fetchUserClassificationRules = () => {
    const self = this;
    self.projectId = self.activatedRoute.snapshot.params['projectId'];
    self.projectService.getClassificationRules(self.projectId, RuleType.user)
      .subscribe(rules => {
        self.userClassificationRules = rules;
        self.fetchBusinessUnits();
      }, error => {
        // TODO: Add error handling
        console.error(error);
      });
  }

  fetchDeviceClassificationRules = () => {
    const self = this;
    self.projectId = self.activatedRoute.snapshot.params['projectId'];
    self.projectService.getClassificationRules(self.projectId, RuleType.device)
      .subscribe(rules => {
        self.deviceClassificationRules = rules;
        self.fetchBusinessUnits();
      }, error => {
        // TODO: Add error handling
        console.error(error);
      });
  }

  fetchUserQualificationRules = () => {
    const self = this;
    self.projectId = self.activatedRoute.snapshot.params['projectId'];
    self.projectService.getQualificationRules(self.projectId, RuleType.user)
      .subscribe(rules => {
        self.userQualificationRules = rules;
        self.fetchBusinessUnits();
      }, error => {
        // TODO: Add error handling
        console.error(error);
      });
  }

  fetchDeviceQualificationRules = () => {
    const self = this;
    self.projectId = self.activatedRoute.snapshot.params['projectId'];
    self.projectService.getQualificationRules(self.projectId, RuleType.device)
      .subscribe(rules => {
        self.deviceQualificationRules = rules;
        self.fetchBusinessUnits();
      }, error => {
        // TODO: Add error handling
        console.error(error);
      });
  }

  saveClassificationRule = (rule: ClassificationRule) => {
    const self = this;
    self.projectService.saveClassificationRule(rule)
      .subscribe(() => {
        if (rule.ruleType === RuleType.device) {
          self.fetchDeviceClassificationRules();
        }

        if (rule.ruleType === RuleType.user) {
          self.fetchUserClassificationRules();
        }
      });
  }

  deleteClassificationRule = (rule: ClassificationRule) => {
    const self = this;
    self.projectService.deleteClassificationRule(rule.id)
      .subscribe(() => {
        if (rule.ruleType === RuleType.device) {
          self.fetchDeviceClassificationRules();
        }

        if (rule.ruleType === RuleType.user) {
          self.fetchUserClassificationRules();
        }
      });
  }

  saveQualificationRule = (rule: QualificationRule) => {
    const self = this;
    self.projectService.saveQualificationRule(rule)
      .subscribe(() => {
        if (rule.ruleType === RuleType.device) {
          self.fetchDeviceQualificationRules();
        }

        if (rule.ruleType === RuleType.user) {
          self.fetchUserQualificationRules();
        }
      });
  }

  deleteQualificationRule = (rule: QualificationRule) => {
    const self = this;
    self.projectService.deleteQualificationRule(rule.id)
      .subscribe(() => {
        if (rule.ruleType === RuleType.device) {
          self.fetchDeviceQualificationRules();
        }

        if (rule.ruleType === RuleType.user) {
          self.fetchUserQualificationRules();
        }
      });
  }

  fetchUserInventoryImportResult = () => {
    const self = this;
    self.importService.getUserInventoryImportResult(self.projectId)
      .subscribe(result => {
        self.userInventoryImportResult = result;
      }, error => {
        // TODO: Add error handling
        console.error(error);
      });
  }

  fetchDeviceInventoryImportResult = () => {
    const self = this;
    self.importService.getDeviceInventoryImportResult(self.projectId)
      .subscribe(result => {
        self.deviceInventoryImportResult = result;
      }, error => {
        // TODO: Add error handling
        console.error(error);
      });
  }
}

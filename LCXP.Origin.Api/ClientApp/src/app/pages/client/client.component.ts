import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Project } from 'models';
import { ProjectService } from 'src/services/project.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css'],
  providers: [ProjectService]
})
export class ClientComponent implements OnInit {

  clientId: string;
  projects: Project[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private projectService: ProjectService
  ) {
  }

  ngOnInit() {
    const self = this;
    self.clientId = self.activatedRoute.snapshot.params['clientId'];
    if (self.clientId) {
      self.projectService.getProjectsForClient(self.clientId)
        .subscribe(projects => {
          self.projects = projects;
        }, error => {
          // TODO: Add error handling
          console.error(error);
        });
    }
  }
}

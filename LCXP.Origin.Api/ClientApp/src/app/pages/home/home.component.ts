import { Component, OnInit } from '@angular/core';
import { ClientService } from 'src/services/client.service';
import { Client } from 'models';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ClientService]
})
export class HomeComponent implements OnInit {

  clients: Client[];

  constructor(
    private clientService: ClientService
  ) { }

  ngOnInit() {
    const self = this;
    self.clientService.getAllClients()
      .subscribe(clients => {
        self.clients = clients;
      }, error => {
        // TODO: Add Error handling
        console.error(error);
      });
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// Material UI // TODO: NB: Optimize material module imports
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressBarModule } from '@angular/material/progress-bar';
// Material UI
import { AppComponent } from './app.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { HomeComponent } from './pages/home/home.component';
import { ClientComponent } from './pages/client/client.component';
import { AppRoutingModule } from './app-routing.module';
import { DataImportResultTabComponent } from './components/data-import-result-tab/data-import-result-tab.component';
import { ProjectComponent } from './pages/project/project.component';
import { ActiveDirectoryModuleComponent } from './pages/module/active-directory-module/active-directory-module.component';
import { ProjectConfigTabComponent } from './components/project-config-tab/project-config-tab.component';
import { AdClassificationRulesTabComponent } from './components/ad-classification-rules-tab/ad-classification-rules-tab.component';
import { AdQualificationRulesTabComponent } from './components/ad-qualification-rules-tab/ad-qualification-rules-tab.component';
import { TitleCasePipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    ClientComponent,
    DataImportResultTabComponent,
    ProjectComponent,
    ActiveDirectoryModuleComponent,
    ProjectConfigTabComponent,
    AdClassificationRulesTabComponent,
    AdQualificationRulesTabComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatListModule,
    MatTreeModule,
    MatIconModule,
    MatCheckboxModule,
    MatStepperModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatDividerModule,
    MatCardModule,
    MatGridListModule,
    MatTabsModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatProgressBarModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [TitleCasePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }

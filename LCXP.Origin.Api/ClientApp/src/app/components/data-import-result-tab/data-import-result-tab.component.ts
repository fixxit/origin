import { Component, OnInit, ViewChild, ElementRef, OnChanges, Input } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-data-import-result-tab',
  templateUrl: './data-import-result-tab.component.html',
  styleUrls: ['./data-import-result-tab.component.css']
})
export class DataImportResultTabComponent implements OnInit, OnChanges {

  @Input() dataImportResult: any[];
  @Input() fetchImportResult: () => void;
  @Input() dataType: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  selectedRows = [];
  isLoading = false;
  selection = new SelectionModel<any>(true, []);
  dataSource: MatTableDataSource<any>;
  filterColumnName = '';
  displayedColumns: string[] = [];

  // metrics
  deviceState = [];
  deviceStatus = [];
  deviceType = [];
  userState = [];
  userStatus = [];

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.dataImportResult.length > 0) {
      const keys = Object.keys(this.dataImportResult[0]);
      this.dataSource = new MatTableDataSource(this.dataImportResult);
      keys.forEach(key => { this.displayedColumns.push(key); });
      this.isLoading = false;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.filterPredicate = this.filterColumn;

      if (this.isDeviceData()) {
        this.deviceState = [];
        this.deviceStatus = [];
        this.deviceType = [];
        const groupedDeviceState = this.groupBy(this.dataImportResult, 'deviceState');
        const groupedDeviceStatus = this.groupBy(this.dataImportResult, 'deviceStatus');
        const groupedDeviceType = this.groupBy(this.dataImportResult, 'deviceType');
        const deviceStateKeys = Object.keys(groupedDeviceState);
        const deviceStatusKeys = Object.keys(groupedDeviceStatus);
        const deviceTypeKeys = Object.keys(groupedDeviceType);
        deviceStateKeys.forEach(key => { this.deviceState.push({ key, value: groupedDeviceState[key].length }); });
        deviceStatusKeys.forEach(key => { this.deviceStatus.push({ key, value: groupedDeviceStatus[key].length }); });
        deviceTypeKeys.forEach(key => { this.deviceType.push({ key, value: groupedDeviceType[key].length }); });
      }

      if (this.isUserData()) {
        this.userState = [];
        this.userStatus = [];
        const groupedUserState = this.groupBy(this.dataImportResult, 'userState');
        const groupedUserStatus = this.groupBy(this.dataImportResult, 'userStatus');
        const userStateKeys = Object.keys(groupedUserState);
        const userStatusKeys = Object.keys(groupedUserStatus);
        userStateKeys.forEach(key => { this.userState.push({ key, value: groupedUserState[key].length }); });
        userStatusKeys.forEach(key => { this.userStatus.push({ key, value: groupedUserStatus[key].length }); });
      }
    }
  }

  getImportResult = () => {
    this.displayedColumns = [];
    this.isLoading = true;
    this.fetchImportResult();
  }

  applyFilter = (filterValue: string, filterColumnName: string) => {
    this.filterColumnName = filterColumnName;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  filterColumn = (data: any, filter: string): boolean => {
    const value = data[this.filterColumnName];
    const stringValue = value ? value.toString() : null;
    if (!stringValue) {
      return false;
    }

    const index = stringValue
      .trim()
      .toLowerCase()
      .indexOf(filter);
    return index > -1;
    const e = document.getElementsByClassName('react-grid-HeaderRow');
    (e[0] as any).style.overflowX = 'visible'; // hack to get React component to place nicely
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataImportResult.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataImportResult.forEach(row => this.selection.select(row));
  }

  isDeviceData = () => {
    return this.dataType === 'device';
  }

  isUserData = () => {
    return this.dataType === 'user';
  }

  groupBy = (xs, key) => {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  }
}

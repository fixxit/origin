import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ClassificationRule, RuleType, BusinessUnit } from 'models';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';

@Component({
  selector: 'app-ad-classification-rules-tab',
  templateUrl: './ad-classification-rules-tab.component.html',
  styleUrls: ['./ad-classification-rules-tab.component.css']
})
export class AdClassificationRulesTabComponent implements OnInit, OnChanges {

  @Input() fetchRules: () => void;
  @Input() saveRule: (rule: ClassificationRule) => void;
  @Input() deleteRule: (rule: ClassificationRule) => void;
  @Input() projectId: string;
  @Input() defaultRuleType: RuleType;
  @Input() rules: ClassificationRule[];
  @Input() businessUnits: BusinessUnit[];

  public rulesForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.fetchRules();
  }

  ngOnChanges() {
    const self = this;
    self.rulesForm = self.formBuilder.group({ rulesArray: self.formBuilder.array([]) });
    if (self.rules !== null && self.rules.length > 0) {
      const formArray = self.rulesForm.get('rulesArray') as FormArray;
      self.rules.forEach(rule => {
        formArray.push(self.createRuleFormControl(rule));
      });
    }
  }

  addRule = () => {
    const newEmptyRule = new ClassificationRule(0, '', '', '', '', '', '', '', '', '', this.defaultRuleType, this.projectId, '');
    const formArray = this.rulesForm.get('rulesArray') as FormArray;
    formArray.push(this.createRuleFormControl(newEmptyRule));
    this.rules.push(newEmptyRule);
  }

  saveRules = () => {
    // Todo: Add proper data validation
    const self = this;
    const formArray = self.rulesForm.get('rulesArray') as FormArray;
    if (formArray.controls.length === self.rules.length && self.rules.length > 0) {
      formArray.controls.forEach((control, i) => {
        const rule = self.rules[i];
        const classificationId = self.defaultRuleType === RuleType.device ? `DC${i}` : `UC${i}`;
        const typeValue = control.get('type').value;
        const operatorValue = control.get('operator').value;
        const objectValue = control.get('object').value;
        const sourceValue = control.get('source').value;
        const setRoleToValue = control.get('setRoleTo').value;
        const setTypeToValue = control.get('setTypeTo').value;
        const setClassToValue = control.get('setClassTo').value;
        const setStateToValue = control.get('setStateTo').value;
        const businessUnitIdValue = control.get('businessUnitId').value;

        const classificationTargetValue = this.isDeviceRule() ? control.get('classificationTarget').value : '';
        const deviceTypeValue = this.isDeviceRule() ? control.get('deviceType').value : '';
        const setEnvironmentToValue = this.isDeviceRule() ? control.get('setEnvironmentTo').value : '';

        rule.classificationId = classificationId;
        rule.type = typeValue;
        rule.operator = operatorValue;
        rule.object = objectValue;
        rule.source = sourceValue;
        rule.setRoleTo = setRoleToValue;
        rule.setTypeTo = setTypeToValue;
        rule.setClassTo = setClassToValue;
        rule.setStateTo = setStateToValue;
        rule.businessUnitId = businessUnitIdValue;
        rule.classificationTarget = classificationTargetValue;
        rule.deviceType = deviceTypeValue;
        rule.setEnvironmentTo = setEnvironmentToValue;

        self.saveRule(rule);
      });
    }
  }

  removeRule = (i: number) => {
    const rule = this.rules[i];
    if (rule.id > 0) {
      this.deleteRule(rule);
    }

    this.rules.splice(i, 1);
    const formArray = this.rulesForm.get('rulesArray') as FormArray;
    formArray.controls.splice(i, 1);
  }

  createRuleFormControl = (rule: ClassificationRule): FormGroup => {
    return this.formBuilder.group({
      type: [rule.type],
      operator: [rule.operator],
      object: [rule.object],
      source: [rule.source],
      setRoleTo: [rule.setRoleTo],
      setTypeTo: [rule.setTypeTo],
      setClassTo: [rule.setClassTo],
      setStateTo: [rule.setStateTo],
      businessUnitId: [rule.businessUnitId],
      // Device specific
      classificationTarget: [this.isDeviceRule() ? rule.classificationTarget : ''],
      deviceType: [this.isDeviceRule() ? rule.deviceType : ''],
      setEnvironmentTo: [this.isDeviceRule() ? rule.setEnvironmentTo : '']
    });
  }

  isDeviceRule = (): boolean => {
    return this.defaultRuleType === RuleType.device;
  }

}

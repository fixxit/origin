import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Configuration, BusinessUnit } from 'models';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';

const emptyGuid = '00000000-0000-0000-0000-000000000000';

@Component({
  selector: 'app-project-config-tab',
  templateUrl: './project-config-tab.component.html',
  styleUrls: ['./project-config-tab.component.css']
})
export class ProjectConfigTabComponent implements OnInit, OnChanges {

  @Input() fetchConfiguration: () => void;
  @Input() fetchBusinessUnits: () => void;
  @Input() saveConfiguration: (configuration: Configuration) => void;
  @Input() deleteBusinessUnit: (id: string) => void;
  @Input() existingConfig: Configuration;
  @Input() businessUnits: BusinessUnit[];

  public configForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.fetchConfiguration();
    this.fetchBusinessUnits();
  }

  ngOnChanges() {
    const self = this;
    if (self.existingConfig !== null) {
      const referenceDate = new Date(self.existingConfig.referenceDate).toISOString().substr(0, 10);
      const snapshotDate = new Date(self.existingConfig.snapshotDate).toISOString().substr(0, 10);
      let buArray: FormArray;
      if (self.businessUnits) {
        buArray = self.createBusinessUnitFormArray(self.businessUnits);
      }

      self.configForm = self.formBuilder.group({
        parent: [self.existingConfig.parent],
        managedManufacturers: [self.existingConfig.managedManufacturers],
        dateFormat: [self.existingConfig.dateFormat],
        referenceDate: [referenceDate],
        snapshotDate: [snapshotDate],
        filePath: [self.existingConfig.filePath],
        businessUnitArray: buArray
      });
    }
  }

  createBusinessUnitFormArray = (businessUnits: BusinessUnit[]): FormArray => {
    const self = this;
    const array = self.formBuilder.array([]);
    if (businessUnits) {
      businessUnits.forEach(businessUnit => {
        array.push(self.createBusinessUnitFormControl(businessUnit));
      });
    }

    return array;
  }

  createBusinessUnitFormControl = (businessUnit: BusinessUnit) => {
    return this.formBuilder.group({
      name: [businessUnit.name],
      city: [businessUnit.city],
      country: [businessUnit.country],
      continent: [businessUnit.continent],
      region: [businessUnit.region]
    });
  }

  saveConfig = () => {
    // TODO: Add data validation
    const self = this;
    const parentValue = self.configForm.get('parent').value;
    const managedManufacturersValue = self.configForm.get('managedManufacturers').value;
    const dateFormatValue = self.configForm.get('dateFormat').value;
    const referenceDateValue = self.configForm.get('referenceDate').value;
    const snapshotDateValue = self.configForm.get('snapshotDate').value;
    const filePathValue = self.configForm.get('filePath').value;

    const updatedBusinessUnits: BusinessUnit[] = [];
    const businessUnitArray = self.configForm.get('businessUnitArray') as FormArray;
    if (businessUnitArray.controls.length === self.businessUnits.length
      && self.businessUnits.length > 0) {
      businessUnitArray.controls.forEach((control, i) => {
        const businessUnit = self.businessUnits[i];
        const nameValue = control.get('name').value;
        const cityValue = control.get('city').value;
        const countryValue = control.get('country').value;
        const continentValue = control.get('continent').value;
        const regionValue = control.get('region').value;

        businessUnit.name = nameValue;
        businessUnit.city = cityValue;
        businessUnit.country = countryValue;
        businessUnit.continent = continentValue;
        businessUnit.region = regionValue;

        updatedBusinessUnits.push(businessUnit);
      });
    }

    const updatedConfiguration = new Configuration(
      self.existingConfig.id,
      parentValue,
      managedManufacturersValue,
      dateFormatValue,
      new Date(referenceDateValue),
      new Date(snapshotDateValue),
      filePathValue,
      self.existingConfig.projectId,
      updatedBusinessUnits);

    self.saveConfiguration(updatedConfiguration);
  }

  addBusinessUnit = () => {
    const newEmptyBusinessUnit = new BusinessUnit(emptyGuid, '', '', '', '', '', this.existingConfig.id);
    const businessUnitArray = this.configForm.get('businessUnitArray') as FormArray;
    businessUnitArray.push(this.createBusinessUnitFormControl(newEmptyBusinessUnit));
    if (this.businessUnits == null || this.businessUnits === undefined) {
      this.businessUnits = [];
    }

    this.businessUnits.push(newEmptyBusinessUnit);
  }

  deleteClick = (i: number) => {
    const businessUnit = this.businessUnits[i];
    if (businessUnit.id !== emptyGuid) {
      this.deleteBusinessUnit(businessUnit.id);
    }

    this.businessUnits.splice(i, 1);
    const businessUnitArray = this.configForm.get('businessUnitArray') as FormArray;
    businessUnitArray.controls.splice(i, 1);
  }
}

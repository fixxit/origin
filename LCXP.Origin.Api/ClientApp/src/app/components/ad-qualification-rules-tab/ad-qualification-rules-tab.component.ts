import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { QualificationRule, RuleType, BusinessUnit } from 'models';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';

@Component({
  selector: 'app-ad-qualification-rules-tab',
  templateUrl: './ad-qualification-rules-tab.component.html',
  styleUrls: ['./ad-qualification-rules-tab.component.css']
})
export class AdQualificationRulesTabComponent implements OnInit, OnChanges {

  @Input() fetchRules: () => void;
  @Input() saveRule: (rule: QualificationRule) => void;
  @Input() deleteRule: (rule: QualificationRule) => void;
  @Input() projectId: string;
  @Input() defaultRuleType: RuleType;
  @Input() rules: QualificationRule[];
  @Input() businessUnits: BusinessUnit[];

  public rulesForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.fetchRules();
  }

  ngOnChanges() {
    const self = this;
    self.rulesForm = self.formBuilder.group({ rulesArray: self.formBuilder.array([]) });
    if (self.rules !== null && self.rules.length > 0) {
      const formArray = self.rulesForm.get('rulesArray') as FormArray;
      self.rules.forEach(rule => {
        formArray.push(self.createRuleFormControl(rule));
      });
    }
  }

  addRule = () => {
    const newEmptyRule = new QualificationRule(0, '', '', '', '', '', '', '', '', '', '', this.defaultRuleType, this.projectId, '');
    const formArray = this.rulesForm.get('rulesArray') as FormArray;
    formArray.push(this.createRuleFormControl(newEmptyRule));
    this.rules.push(newEmptyRule);
  }

  saveRules = () => {
    // TODO: Add data validation
    const self = this;
    const formArray = self.rulesForm.get('rulesArray') as FormArray;
    if (formArray.controls.length === self.rules.length && self.rules.length > 0) {
      formArray.controls.forEach((control, i) => {
        const rule = self.rules[i];
        const qualificationId = control.get('qualificationId').value;
        const typeValue = control.get('type').value;
        const operatorValue = control.get('operator').value;
        const objectValue = control.get('object').value;
        const conditionValue = control.get('condition').value;
        const sourceValue = control.get('source').value;
        const setUserCountToValue = control.get('setUserCountTo').value;
        const setUserStateToValue = control.get('setUserStateTo').value;
        const setUserStatusToValue = control.get('setUserStatusTo').value;
        const setExclusionReasonToValue = control.get('setExclusionReasonTo').value;
        const businessUnitIdValue = control.get('businessUnitId').value;

        const setDeviceStateToValue = this.isDeviceRule() ? control.get('setDeviceStateTo').value : '';
        const setDeviceStatusToValue = this.isDeviceRule() ? control.get('setDeviceStatusTo').value : '';
        const setOfficeEligibleDeviceToValue = this.isDeviceRule() ? control.get('setOfficeEligibleDeviceTo').value : '';
        const setObservationTypeToValue = this.isDeviceRule() ? control.get('setObservationTypeTo').value : '';
        const setObservationToValue = this.isDeviceRule() ? control.get('setObservationTo').value : '';
        const setActionToValue = this.isDeviceRule() ? control.get('setActionTo').value : '';

        rule.qualificationId = qualificationId;
        rule.type = typeValue;
        rule.operator = operatorValue;
        rule.object = objectValue;
        rule.condition = conditionValue;
        rule.source = sourceValue;
        rule.setUserCountTo = setUserCountToValue;
        rule.setUserStateTo = setUserStateToValue;
        rule.setUserStatusTo = setUserStatusToValue;
        rule.setExclusionReasonTo = setExclusionReasonToValue;
        rule.businessUnitId = businessUnitIdValue;

        rule.setDeviceStateTo = setDeviceStateToValue;
        rule.setDeviceStatusTo = setDeviceStatusToValue;
        rule.setOfficeEligibleDeviceTo = setOfficeEligibleDeviceToValue === 'Y' ? true : false;
        rule.setObservationTypeTo = setObservationTypeToValue;
        rule.setObservationTo = setObservationToValue;
        rule.setActionTo = setActionToValue;

        self.saveRule(rule);
      });
    }
  }

  removeRule = (i: number) => {
    const rule = this.rules[i];
    if (rule.id > 0) {
      this.deleteRule(rule);
    }

    this.rules.splice(i, 1);
    const formArray = this.rulesForm.get('rulesArray') as FormArray;
    formArray.controls.splice(i, 1);
  }

  createRuleFormControl = (rule: QualificationRule): FormGroup => {
    return this.formBuilder.group({
      qualificationId: [rule.qualificationId],
      type: [rule.type],
      operator: [rule.operator],
      object: [rule.object],
      condition: [rule.condition],
      source: [rule.source],
      setUserCountTo: [rule.setUserCountTo],
      setUserStateTo: [rule.setUserStateTo],
      setUserStatusTo: [rule.setUserStatusTo],
      setExclusionReasonTo: [rule.setExclusionReasonTo],
      businessUnitId: [rule.businessUnitId],
      // Device specific
      setDeviceStateTo: [this.isDeviceRule() ? rule.setDeviceStateTo : ''],
      setDeviceStatusTo: [this.isDeviceRule() ? rule.setDeviceStatusTo : ''],
      setOfficeEligibleDeviceTo: [this.isDeviceRule() ? this.getOfficeEligibility(rule.setOfficeEligibleDeviceTo) : ''],
      setObservationTypeTo: [this.isDeviceRule() ? rule.setObservationTypeTo : ''],
      setObservationTo: [this.isDeviceRule() ? rule.setObservationTo : ''],
      setActionTo: [this.isDeviceRule() ? rule.setActionTo : '']
    });
  }

  isDeviceRule = (): boolean => {
    return this.defaultRuleType === RuleType.device;
  }

  getOfficeEligibility = (value: boolean): string => {
    if (value === true) {
      return 'Y';
    }

    return 'N';
  }

}

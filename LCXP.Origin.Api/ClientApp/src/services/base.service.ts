import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor(
    private http: HttpClient
  ) { }

  public get<T>(route: String): Observable<T> {
    return this.http.get<T>(`${environment.apiUrl}/${route}`);
  }

  public post<T>(route: String, body: T): Observable<T> {
    return this.http.post<T>(`${environment.apiUrl}/${route}`, body);
  }

  public delete<T>(route: String): Observable<T> {
    return this.http.delete<T>(`${environment.apiUrl}/${route}`);
  }
}

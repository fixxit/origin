import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { Observable } from 'rxjs';
import { Client } from 'models';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  constructor(
    private baseService: BaseService
  ) { }

  public getAllClients(): Observable<Client[]> {
    return this.baseService.get<Client[]>(`client`);
  }

  public createClient(client: Client): Observable<Client> {
    return this.baseService.post<Client>(`client/create`, client);
  }
}

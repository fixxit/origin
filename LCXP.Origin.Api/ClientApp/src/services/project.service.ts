import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { Observable } from 'rxjs';
import { Project, Configuration, ClassificationRule, QualificationRule, RuleType, BusinessUnit } from 'models';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  constructor(
    private baseService: BaseService
  ) { }

  public getProjectsForClient(clientId: string): Observable<Project[]> {
    return this.baseService.get<Project[]>(`project/client/${clientId}`);
  }

  public getProjectConfiguration(projectId: string): Observable<Configuration> {
    return this.baseService.get<Configuration>(`project/configuration/${projectId}`);
  }

  public getBusinessUnits(projectId: string): Observable<BusinessUnit[]> {
    return this.baseService.get<BusinessUnit[]>(`project/configuration/businessUnits/${projectId}`);
  }

  public deleteBusinessUnit(buId: string): Observable<void> {
    return this.baseService.delete<void>(`project/configuration/businessUnit/delete/${buId}`);
  }

  public saveProjectConfiguration(configuration: Configuration): Observable<void> {
    return this.baseService.post<any>('project/configuration/save', configuration);
  }

  public getClassificationRules(projectId: string, ruleType: RuleType): Observable<ClassificationRule[]> {
    return this.baseService.get<ClassificationRule[]>(`project/classificationRules/${projectId}/${ruleType}`);
  }

  public getQualificationRules(projectId: string, ruleType: RuleType): Observable<QualificationRule[]> {
    return this.baseService.get<QualificationRule[]>(`project/qualificationRules/${projectId}/${ruleType}`);
  }

  public saveClassificationRule(rule: ClassificationRule): Observable<ClassificationRule> {
    return this.baseService.post<ClassificationRule>('project/classificationRule/save', rule);
  }

  public deleteClassificationRule(ruleId: number): Observable<void> {
    return this.baseService.delete<void>(`project/classificationRule/delete/${ruleId}`);
  }

  public saveQualificationRule(rule: QualificationRule): Observable<QualificationRule> {
    return this.baseService.post<QualificationRule>('project/qualificationRule/save', rule);
  }

  public deleteQualificationRule(ruleId: number): Observable<void> {
    return this.baseService.delete<void>(`project/qualificationRule/delete/${ruleId}`);
  }
}

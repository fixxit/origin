import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { Observable } from 'rxjs';
import { UserInventory, DeviceInventory } from 'models';

@Injectable({
  providedIn: 'root'
})
export class ImportService {
  constructor(
    private baseService: BaseService
  ) { }

  public getUserInventoryImportResult(projectId: string): Observable<UserInventory[]> {
    return this.baseService.get<UserInventory[]>(`import/userInventory/result/${projectId}`);
  }

  public getDeviceInventoryImportResult(projectId: string): Observable<DeviceInventory[]> {
    return this.baseService.get<DeviceInventory[]>(`import/deviceInventory/result/${projectId}`);
  }
}

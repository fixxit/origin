import { RuleType } from './ruleType';

export class QualificationRule {
  public id: number;
  public qualificationId: string;
  public type: string;
  public operator: string;
  public object: string;
  public condition: string;
  public source: string;
  public setUserCountTo: string;
  public setUserStateTo: string;
  public setUserStatusTo: string;
  public setExclusionReasonTo: string;
  public ruleType: RuleType;
  public projectId: string;
  public businessUnitId: string;

  // Device specific
  public setDeviceStateTo: string;
  public setDeviceStatusTo: string;
  public setOfficeEligibleDeviceTo: boolean;
  public setObservationTypeTo: string;
  public setObservationTo: string;
  public setActionTo: string;

  constructor(
    id: number,
    qualificationId: string,
    type: string,
    operator: string,
    object: string,
    condition: string,
    source: string,
    setUserCountTo: string,
    setUserStateTo: string,
    setUserStatusTo: string,
    setExclusionReasonTo: string,
    ruleType: RuleType,
    projectId: string,
    businessUnitId: string
  ) {
    this.id = id;
    this.qualificationId = qualificationId;
    this.type = type;
    this.operator = operator;
    this.object = object;
    this.condition = condition;
    this.source = source;
    this.setUserCountTo = setUserCountTo;
    this.setUserStateTo = setUserStateTo;
    this.setUserStatusTo = setUserStatusTo;
    this.setExclusionReasonTo = setExclusionReasonTo;
    this.ruleType = ruleType;
    this.projectId = projectId;
    this.businessUnitId = businessUnitId;
  }
}

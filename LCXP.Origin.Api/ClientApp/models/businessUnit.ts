export class BusinessUnit {
  public id: string;
  public name: string;
  public city: string;
  public country: string;
  public continent: string;
  public region: string;
  public configurationId: string;

  constructor(
    id: string,
    name: string,
    city: string,
    country: string,
    continent: string,
    region: string,
    configurationId: string
  ) {
    this.id = id;
    this.name = name;
    this.city = city;
    this.country = country;
    this.continent = continent;
    this.region = region;
    this.configurationId = configurationId;
  }
}

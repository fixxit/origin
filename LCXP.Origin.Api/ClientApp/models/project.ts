import { BusinessUnit } from './businessUnit';
import { Configuration } from './configuration';

export class Project {
  public id: string;
  public name: string;
  public commencementDate: Date;
  public businessUnits: BusinessUnit[];
  public configuration: Configuration;

  constructor(id: string, name: string, commencementDate: Date, businessUnits: BusinessUnit[] = [], configuration: Configuration = null) {
    this.id = id;
    this.name = name;
    this.commencementDate = commencementDate;
    this.businessUnits = businessUnits;
    this.configuration = configuration;
  }
}

import { Project } from './project';

export class Client {
  public id: string;
  public name: string;
  public projects: Project[];

  constructor(id: string, name: string, projects: Project[] = []) {
    this.id = id;
    this.name = name;
    this.projects = projects;
  }
}

import { RuleType } from './ruleType';

export class ClassificationRule {
  public id: number;
  public classificationId: string;
  public type: string;
  public operator: string;
  public object: string;
  public source: string;
  public setRoleTo: string;
  public setTypeTo: string;
  public setClassTo: string;
  public setStateTo: string;
  public ruleType: RuleType;
  public projectId: string;
  public businessUnitId: string;

  // Device specific
  public setBusinessUnitTo: string;
  public setCityTo: string;
  public setCountryTo: string;
  public setContinentTo: string;
  public setRegionTo: string;
  public classificationTarget: string;
  public deviceType: string;
  public setEnvironmentTo: string;

  constructor(
    id: number,
    classificationId: string,
    type: string,
    operator: string,
    object: string,
    source: string,
    setRoleTo: string,
    setTypeTo: string,
    setClassTo: string,
    setStateTo: string,
    ruleType: RuleType,
    projectId: string,
    businessUnitId: string
  ) {
    this.id = id;
    this.classificationId = classificationId;
    this.type = type;
    this.operator = operator;
    this.object = object;
    this.source = source;
    this.setRoleTo = setRoleTo;
    this.setTypeTo = setTypeTo;
    this.setClassTo = setClassTo;
    this.setStateTo = setStateTo;
    this.ruleType = ruleType;
    this.projectId = projectId;
    this.businessUnitId = businessUnitId;
  }
}

import { BusinessUnit } from './businessUnit';

export class Configuration {
  public id: string;
  public parent: string;
  public managedManufacturers: string;
  public dateFormat: string;
  public referenceDate: Date;
  public snapshotDate: Date;
  public filePath: string;
  public projectId: string;
  public businessUnits: BusinessUnit[];

  constructor(
    id: string,
    parent: string,
    managedManufacturers: string,
    dateFormat: string,
    referenceDate: Date,
    snapshotDate: Date,
    filePath: string,
    projectId: string,
    businessUnits: BusinessUnit[] = []
  ) {
    this.id = id;
    this.parent = parent;
    this.managedManufacturers = managedManufacturers;
    this.dateFormat = dateFormat;
    this.referenceDate = referenceDate;
    this.snapshotDate = snapshotDate;
    this.filePath = filePath;
    this.projectId = projectId;
    this.businessUnits = businessUnits;
  }
}

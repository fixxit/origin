﻿using LCXP.Origin.Domain.Project;
using LCXP.Origin.Domain.Project.ActiveDirectory;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace LCXP.Origin.Api.Controllers
{
    [EnableCors("AllPolicy")]
    [Route("api/project")]
    [Produces("application/json")]
    [ApiController]
    public class ProjectController : Controller
    {
        private readonly IProjectRepository _projectRepository;

        public ProjectController(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        [HttpGet, AllowAnonymous]
        [Route("client/{clientId}")]
        public IActionResult GetClientProjects(Guid clientId)
        {
            var projects = _projectRepository.GetClientProjects(clientId);
            return Json(projects);
        }

        [HttpGet, AllowAnonymous]
        [Route("configuration/{projectId}")]
        public IActionResult GetConfigurationForProject(Guid projectId)
        {
            var configuration = _projectRepository.GetConfiguration(projectId);
            return Json(configuration);
        }

        [HttpGet, AllowAnonymous]
        [Route("configuration/businessUnits/{projectId}")]
        public IActionResult GetBusinessUnitsForProject(Guid projectId)
        {
            var businessUnits = _projectRepository.GetBusinessUnits(projectId);
            return Json(businessUnits);
        }

        [HttpDelete, AllowAnonymous]
        [Route("configuration/businessUnit/delete/{businessUnitId}")]
        public IActionResult DeleteBusinessUnit(Guid businessUnitId)
        {
            _projectRepository.DeleteBusinessUnit(businessUnitId);
            return Ok();
        }

        [HttpPost, AllowAnonymous]
        [Route("configuration/save")]
        public IActionResult SaveConfiguration(Configuration configuration)
        {
            // TODO: Validate model
            var updatedConfiguration = _projectRepository.SaveConfiguration(configuration);
            return Ok();
        }

        [HttpGet, AllowAnonymous]
        [Route("classificationRules/{projectId}/{ruleType}")]
        public IActionResult GetClassificationRules(Guid projectId, RuleType ruleType)
        {
            var classificationRules = _projectRepository.GetClassificationRules(projectId, ruleType).Take(15);
            return Json(classificationRules);
        }

        [HttpPost, AllowAnonymous]
        [Route("classificationRule/save")]
        public IActionResult SaveClassificationRule(ClassificationRule classificationRule)
        {
            // TODO: Add data validation
            var savedRule = _projectRepository.SaveClassificationRule(classificationRule);
            return Json(savedRule);
        }

        [HttpDelete, AllowAnonymous]
        [Route("classificationRule/delete/{ruleId}")]
        public IActionResult DeleteClassificationRule(long ruleId)
        {
            _projectRepository.DeleteClassificationRule(ruleId);
            return Ok();
        }

        [HttpGet, AllowAnonymous]
        [Route("qualificationRules/{projectId}/{ruleType}")]
        public IActionResult GetQualificationRules(Guid projectId, RuleType ruleType)
        {
            var qualificationRules = _projectRepository.GetQualificationRules(projectId, ruleType).Take(15);
            return Json(qualificationRules);
        }

        [HttpPost, AllowAnonymous]
        [Route("qualificationRule/save")]
        public IActionResult SaveQualificationRule(QualificationRule qualificationRule)
        {
            var savedRule = _projectRepository.SaveQualificationRule(qualificationRule);
            return Json(savedRule);
        }

        [HttpDelete, AllowAnonymous]
        [Route("qualificationRule/delete/{ruleId}")]
        public IActionResult DeleteQualificationRule(long ruleId)
        {
            _projectRepository.DeleteQualificationRule(ruleId);
            return Ok();
        }
    }
}

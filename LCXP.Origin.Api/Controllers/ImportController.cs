﻿using LCXP.Origin.Domain;
using LCXP.Origin.Domain.ActiveDirectory;
using LCXP.Origin.Domain.Import;
using LCXP.Origin.Domain.Project;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;

namespace LCXP.Origin.Api.Controllers
{
    [EnableCors("AllPolicy")]
    [Route("api/import")]
    [Produces("application/json")]
    [ApiController]
    public class ImportController : Controller
    {
        private readonly IImportRepository _importRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly OriginContext _originContext;

        public ImportController(IImportRepository importRepository, IProjectRepository projectRepository, OriginContext originContext)
        {
            _importRepository = importRepository;
            _projectRepository = projectRepository;
            _originContext = originContext;
        }

        [HttpGet, AllowAnonymous]
        [Route("userInventory/result/{projectId}")]
        public IActionResult GetUserInventoryImportResult(Guid projectId)
        {
            var filePath = _projectRepository.GetConfiguration(projectId).FilePath;
            var importer = new ImportUsers(_originContext, filePath);
            importer.ImportUserInventory();
            var result = _importRepository.GetUserInventoryImportResult(projectId);
            return Json(result);
        }

        [HttpGet, AllowAnonymous]
        [Route("deviceInventory/result/{projectId}")]
        public IActionResult GetDeviceInventoryImportResult(Guid projectId)
        {
            var filePath = _projectRepository.GetConfiguration(projectId).FilePath;
            var importer = new ImportDevices(_originContext, filePath);
            importer.ImportDeviceInventory();
            var result = _importRepository.GetDeviceInventoryImportResult(projectId);
            return Json(result);
        }
    }
}

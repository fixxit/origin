﻿using LCXP.Origin.Domain.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;

namespace LCXP.Origin.Api.Controllers
{
    [EnableCors("AllPolicy")]
    [Route("api/client")]
    [Produces("application/json")]
    [ApiController]
    public class ClientController : Controller
    {
        private readonly IClientRepository _clientRepository;

        public ClientController(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        [HttpGet, AllowAnonymous]
        public IActionResult GetAllClients()
        {
            // TODO: Add paging if necessary
            var clients = _clientRepository.GetAllClients();
            return Ok(clients);
        }

        [HttpGet, AllowAnonymous]
        [Route("{clientId}")]
        public ActionResult GetClient(Guid clientId)
        {
            var client = _clientRepository.GetClient(clientId);
            return Ok(client);
        }

        [HttpPost, AllowAnonymous]
        [Route("create")]
        public ActionResult CreateClient(Client client)
        {
            // TODO: Add data validation
            var savedClient = _clientRepository.CreateClient(client);
            return Json(savedClient);
        }
    }
}
